/*���� ������� ��������� ���������������� �����.
���������, �������� �� ���� �������-����. �� ���� �������, � ������� ����� ����� �� ���� ������,
� �� ������� �� ������� �� ������ �����. ��������� ��������� O(V).*/

#include <iostream>
using namespace std;

class MyGraph
{
private:
	int n; // ������ ������� ��������� ��� ���-�� ������ � �����
	int ** mas; // ������� ���������
public:
	MyGraph() {};
	MyGraph(int a);
	~MyGraph();
	bool HasStock(); // ������� ��� �������� �����
	bool IsStock(int i); // ������� ��� ��������� ���������� ��� ����������� �����
	void InputEdge(int i, int j, int ToAdd);
};

MyGraph::~MyGraph() // ������� ��������� ������������ �������
{
	for (int i = 0; i < n; ++i)
		delete[] mas[i];
	delete[] mas;
}

bool MyGraph::HasStock()
{
	bool* CanBe = new bool[n]; // ����� ������, � ������� ����� ��������� ����� �� ���� ������ ������� ������ ��� ���
	for (int i = 0; i < n; ++i) // ����� �������, ��� ���������� ��� ������� ����� ���� ������
		CanBe[i] = true;

	int i = 0, j = 1; // � ����� (0,0) �������� ������������, ��� ��� �� �� ����� ��������� ������
	while ((i != (n - 1)) && (j != (n - 1))) // �� ��� ���, ���� �� ����� �� ������� �������
	{
		if (mas[i][j] == 0) // ������, ����� 0
		{
			if (i != j) // ����� ����������, ������� ������ ���������� ��� �� ����� ���� �����, �������� �� ��� �������
				CanBe[j] = false; // � ������ ������� ������ ��������� ��� ���� ������, ��� ������� j - �� ����
			j += 1; // ��� ������, ��� ��� ������ ��� ����� ���� ��� �� ����������� �������
		}
		else  // ������, ����� 1
		{
			CanBe[i] = false;  // ������ ��������� ��� ���� ������, ��� ������� i - �� ����
			i = j; // ������ ���� ���� ��� �� ����������� �������
		}
	}
	// �������� 2 ����������
	CanBe[i] = IsStock(i); // �������� �� ������� i < n-1, ��� ������� ���������� ������� �������, ������?
	CanBe[n - 1] = IsStock(n - 1); // �������� �� ��������� ������?
	for (int i = 0; i < n; ++i) // ���������, ���� �� �������, ������� ����� ���� ������. ������� false or true ��������������
	{
		if (CanBe[i] == true)
		{
			delete[] CanBe;
			return true;
		}
	}
	delete[] CanBe;
	return false;
}

bool MyGraph::IsStock(int i)
{
	for (int t = 0; t < n; ++t) // � ����������� ������� ������ ������ ������ 0
	{
		if (mas[i][t] == 1)
			return false;
	}
	for (int t = 0; (t < n); ++t) // � ��������������� ����������� ������ ������ ������ ������ 1, ����� ����������� � ����������� ��������
	{
		if ((mas[t][i] == 0) && (t != i))
			return false;
	}
	return true;
}

MyGraph::MyGraph(int a)
{
	n = a;
	mas = new int*[n]; // �������� ������� ���������
	for (int i = 0; i < n; ++i)
		mas[i] = new int[n];
	for (int i = 0; i < n; ++i) // �������� ������ ������� ������
	{
		for (int j = 0; j < n; ++j)
			mas[i][j] = 0;
	}
}

void MyGraph::InputEdge(int i, int j, int ToAdd)
{
	mas[i][j] = ToAdd;
}

int main()
{
	int n = 0;
	cin >> n; // ����� ���-�� ������ � �����
	MyGraph MatrixGraph(n);
	int ToAdd = 0; // ��������, ������� �� ����� �������� � ������� ���������
	for (int i = 0; i < n; ++i) // �������� ������� ��������� ������� ��� ����������
	{
		for (int j = 0; j < n; ++j)
		{
			cin >> ToAdd;
			MatrixGraph.InputEdge(i, j, ToAdd);
		}
	}
	if (MatrixGraph.HasStock() == true)
		cout << "YES";
	else
		cout << "NO";
	return 0;
}