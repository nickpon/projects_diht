/*������������ ������ ������� � ��� ������� ������ � ������ ����� �������� �������. 
����� �������� ������������ �� ����� ������� �������, ������� ����������� ���OY ������� ���������. 
���� � ������� ��������� �����, � ����� �� �������� � ��� � ������. 
� �������� ���� �������������� (�� ����� �������) ������� ����������� ������� R.
������������ ��������, ������� �� ���������� � �������� ��������, � ���������� ������ ���������� �������
����������� �� ������� ������, ������� ����� �������� ����� ����� �������, ��������
����������� ����� ��������������.*/

#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

class MyGraph
{
private:
	int N; //���������� ������
	int flag;
	vector<vector<long double>> mas;
	vector<int> used;
public:
	MyGraph(int XL, int XR, int R, vector<long double> &X, vector<long double> &Y, long double TableSize);
	MyGraph() {};
	void AddEdge(long double from, long double to); // ���������� ����� � ����
	void dfs(int v); // ������� DFS
	int HasRoute(); // ������ �� ���� �� ����� ����� �� ������?
};

MyGraph::MyGraph(int XL, int XR, int R, vector<long double> &X, vector<long double> &Y, long double TableSize) :
	N(X.size() + 2), mas(X.size() + 2), flag(false), used(X.size() + 2)
{
	for (int i = 0; i < N - 2; ++i)
	{
		for (int j = i + 1; j < (N - 2); ++j)
		{
			long double root = sqrt((X[j] - X[i]) * (X[j] - X[i]) + (Y[j] - Y[i]) * (Y[j] - Y[i]));
			if ((root - 2 * R) < TableSize) // ��������� ���� �������, ���������� ����� �������� ������ ������� �����
				AddEdge(i, j);
		}
		if (XR - X[i] - R < TableSize) // ��������� ���� ������� � �����, ���������� ����� �������� ������ ������� �����
			AddEdge(N - 2, i);
		if (X[i] - XL - R < TableSize)
			AddEdge(N - 1, i);
	}
}

void MyGraph::AddEdge(long double from, long double to)
{
	mas[to].push_back(from);
	mas[from].push_back(to);
}

void MyGraph::dfs(int v)
{
	if (used[v] == true)
		return;
	used[v] = true;
	for (int i = 0; i < mas[v].size(); ++i)
	{
		int w = mas[v][i];
		if (w == N - 2)
		{
			flag = true;
			break;
		}
		dfs(w);
	}
}

int MyGraph::HasRoute() // ���� DFS'�� �� ����� ����� ����� �� ����� ����� �� ������, �� ���� �������� ������
{
	for (int i = 0; i < mas[N - 1].size(); ++i)
	{
		dfs(mas[N - 1][i]);
		if (flag == true)
		{
			return true;
		}
	}
	return false;
}

long double BinSearch(int R, int XL, int XR, vector<long double> &X, vector<long double> &Y)
{
	long double RightTableSize = XR - XL; // ��������� ������ ������� ���������
	MyGraph graphR(XL, XR, R, X, Y, RightTableSize); // ������ ���� ��� ���� �������� (��. ����������� �����)
	if (graphR.HasRoute() == false) // �������� �� ��, ������ �� ����?
		return RightTableSize; // ������� �� ������������ ������� ����� ����� ��������
	long double LeftTableSize = 0; // ��������� ����� ������� ���������
	MyGraph graphL(XL, XR, R, X, Y, LeftTableSize); // ������ ���� ��� ���� �������� (��. ����������� �����)
	if (graphL.HasRoute() == true)
		return LeftTableSize; // �� �������� �� ���� ����
	while ((RightTableSize - LeftTableSize) > 0.00001) // �������� ���������
	{
		long double MiddleTableSize = (RightTableSize + LeftTableSize) / 2;
		MyGraph graph(XL, XR, R, X, Y, MiddleTableSize);
		if (graph.HasRoute() == false)
			LeftTableSize = MiddleTableSize;
		else
			RightTableSize = MiddleTableSize;
	}
	return LeftTableSize; // ������� ��������� ������ �����
}

int main()
{
	int XL = 0, XR = 0, R = 0, N = 0;
	cin >> XL;
	cin >> XR;
	cin >> R;
	cin >> N;
	vector<long double> X; // ������� ��� �������� ��������� ������
	vector<long double> Y;
	for (int i = 0; i < N; ++i)
	{
		long double x = 0, y = 0;
		cin >> x;
		cin >> y;
		if ((x > XL - R) && (x < XR + R)) // �������� �� �������, ������� ��� ���� �� ��������, �� ���� �� ��������� � ���������
		{
			X.push_back(x);
			Y.push_back(y);
		}
	}
	long double result = BinSearch(R, XL, XR, X, Y); // �������� ������� �����
	printf("%.3llf", result); // �������� � �������� �� 3-� ������
	return 0;
}