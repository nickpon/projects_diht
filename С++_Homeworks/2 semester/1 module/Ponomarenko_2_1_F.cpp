/*��� ��������������� ����.����������, ����� ����������� �����
����� ���������� ��������,����� ���� ���� �������������.*/

#include <iostream>
#include <cmath>
#include <stack>
#include <vector>
using namespace std;

class MyGraph // ����� ������������� �����
{
private:
	vector<vector<int>> mas; // �������� ����� �������� ���������
	vector<int> used; // ���������� � ��������� (���� �������)
	vector<int> numberOrder; // ������� ������
public:
	MyGraph(int n); // ������������� ������
	void AddEdgeEdge(int u, int v); // �������� �����
	void DFS1(int u); // ������� �������, � ������� //������� ����� ����������
	void DFS2(int u); // ���� ����������
	bool is_used(int i) { return used[i]; }; // ��������������� ��?
	void numberOrder_clear() { numberOrder.clear(); }; // �������� numberOrder
	void used_clear(); // �������� used
	int get_mas(int a, int b) { return mas[a][b]; };
	int get_massize(int i) { return mas[i].size(); };
	int get_numberOrder(int i) { return numberOrder[i]; };
	int get_numberOrdersize() { return numberOrder.size(); };
};

MyGraph::MyGraph(int n)
{
	vector<int> New;
	mas.resize(n, New);
	used.resize(n, 0);
	numberOrder.resize(0, -1);
}

void MyGraph::used_clear()
{
	for (int i = 0; i < used.size(); ++i)
		used[i] = 0;
}

void MyGraph::AddEdgeEdge(int u, int v)
{
	mas[u].push_back(v);
}

void MyGraph::DFS1(int v) // �������� �������� DFS
{
	if (used[v] == true)
		return;
	used[v] = true;
	for (int i = 0; i < mas[v].size(); ++i)
	{
		int w = mas[v][i];
		DFS1(w);
	}
	numberOrder.push_back(v);
}

void  MyGraph::DFS2(int v)
{
	if (used[v] == true)
		return;
	used[v] = true;
	numberOrder.push_back(v);
	for (int i = 0; i < mas[v].size(); ++i)
	{
		int w = mas[v][i];
		DFS2(w);
	}
}

class NewMyGraph // �����������
{
private:
	vector<vector<int>> mas;
	vector <vector<int>> second;
public:
	NewMyGraph(int n); // ������������� ������
	void AddEdge(int u, int v); // �������� �������
	void second_AddEdge(int u, int v);
	int VertToPaste(int index); // �����������, ������ ������ ��� ������� � ����� ������ � ������� �������
};

NewMyGraph::NewMyGraph(int n)
{
	vector <int> New;
	mas.resize(n, New);
	second.resize(n, New);
}

void NewMyGraph::AddEdge(int u, int v)
{
	mas[u].push_back(v);
}

void NewMyGraph::second_AddEdge(int u, int v)
{
	second[u].push_back(v);
}

int NewMyGraph::VertToPaste(int index)
{
	//if (index == 0)
	//return 0;
	int a = 0;
	int b = 0;
	for (int i = 0; i < mas.size(); ++i)
	{
		if (mas[i].size() == 0)
			++b;
	}
	for (int i = 0; i < mas.size(); ++i)
	{
		if (second[i].size() == 0)
			++a;
	}
	if (a >= b)
		return a;
	else
		return b;
}

int result(int n, MyGraph& graph1, MyGraph& graph2)
{
	for (int i = 0; i < n; ++i)
	{
		if (graph1.is_used(i) == false)
			graph1.DFS1(i);
	}
	int num = 0;
	vector<int> count(n);
	for (int i = 0; i < n; ++i)
	{
		int u = graph1.get_numberOrder(n - 1 - i);
		if (graph2.is_used(u) == false)
		{
			graph2.DFS2(u);
			for (int i = 0; i < graph2.get_numberOrdersize(); ++i)
				count[graph2.get_numberOrder(i)] = num;
			++num;
			graph2.numberOrder_clear();
		}
	}
	if (num == 1)
		num = 0;
	NewMyGraph GraphConden(num);
	int index = 0; // ����������� ��� ��������������� ����� ������
	for (int i = 0; i < n; ++i)
	{
		for (int j = 0; j < graph2.get_massize(i); ++j)
		{
			if (count[i] != count[graph2.get_mas(i, j)])
			{
				++index;
				GraphConden.second_AddEdge(count[graph2.get_mas(i, j)], count[i]);
				GraphConden.AddEdge(count[i], count[graph2.get_mas(i, j)]);
			}
		}
	}
	return GraphConden.VertToPaste(index);
}

int main()
{
	int n = 0, m = 0;
	cin >> n; // ������ ���-�� �����
	cin >> m; // ������ ���-�� ����
	MyGraph graph1(n); // �������������� ��� �����
	MyGraph graph2(n);
	for (int i = 0; i < m; ++i) // ��������� ��
	{
		int u = 0, v = 0;
		cin >> u;
		cin >> v;
		graph1.AddEdgeEdge(u - 1, v - 1); // �������� 1, ��� ��� ��������� ��� � �������
		graph2.AddEdgeEdge(v - 1, u - 1);
	}
	int answer = result(n, graph1, graph2); // ������� ��� ������ ���������� ������ ���������
	cout << answer;
}