/*��� ������������ ����������������� ����. � ����� ����� ���� ��������� ���������� ����� ����� ������-�� ���������.
������� ���������� ��������� ���������� ����� ����� ��������� ���������. ��������� ��������� O(V+E).
����: v:���-�� ������(����. 50000), n:���-�� �����(����. 200000), n ��� �������� ������, ���� ������ v,
w ��� �������. �����:���������� ���������� ����� �� v � w*/

#include <iostream>
#include <queue>
#include <vector>
using namespace std;
class MyGraph
{
private:
	int n; //���������� ������
	vector<vector<int>> mas; // �������� ����� ������� ���������
	vector<bool> used; // �������� �� �������
	queue<int> q; // ������� ��� Bfs
	vector<int> routes; // ���������� ���������� ����� ��� ������ �������
	vector<int> depth; // ������� ��� ������ �������
public:
	MyGraph() {};
	MyGraph(int a);
	~MyGraph() {};
	void Bfsdepth(int v); // Bfs � ����������� ������� ��� ������ �������
	void BfsRoutes(int v); // Bfs ��� ���������� ���-�� ���������� ����� �� �������� ������� �� ������ ������� 
	int result(int FromEdge, int ToEdge); // ������� ��� ������ ����������
	void InputEdge(int v, int w); // ���������� �������
};

void MyGraph::Bfsdepth(int v)
{
	if (used[v]) // �������� ������� Bfs 
		return;
	q.push(v);
	used[v] = true;
	while (!q.empty())
	{
		v = q.front();
		q.pop();
		int Mindepth = n; // ��� ������ �������� ������� ����������� ������� ������� � ��� ������
		bool Changed = false; // ���������� �� �������?
		for (int i = 0; i < mas[v].size(); ++i)
		{
			if ((depth[mas[v][i]] != -1) && (depth[mas[v][i]] < Mindepth)) // ������� ����������� ������� ����� ������� ������
			{																  // ���� ���� ���� ���� ��������������� �������, �� ������ Changed
				Mindepth = depth[mas[v][i]];
				Changed = true;
			}
			if (used[mas[v][i]])
			{
				continue;
			};
			q.push(mas[v][i]);
			used[mas[v][i]] = true;
		}
		if (Changed == false) // ������ ��� ����� �����
			depth[v] = 1;
		else
			depth[v] = Mindepth + 1; // ����������� ������ ������� ������� � �������� + 1

	}
}

void MyGraph::BfsRoutes(int v)
{
	if (used[v])
		return;
	q.push(v);
	used[v] = true;
	while (!q.empty())
	{
		v = q.front();
		q.pop();
		for (int i = 0; i < mas[v].size(); ++i)
		{
			if (depth[v] < depth[mas[v][i]]) // ��������, ��� ������� �� ����� �� ����� ������
				routes[mas[v][i]] += routes[v]; // ����������, ��� ���������� � ������ ������� ���-�� ���������� ����� � ��������
			if (used[mas[v][i]])
			{
				continue;
			};
			q.push(mas[v][i]);
			used[mas[v][i]] = true;
		}
	}
}

int MyGraph::result(int FromEdge, int ToEdge)
{
	routes[FromEdge] = 1; // ��������� �������
	depth[FromEdge] = 0;
	Bfsdepth(FromEdge); // ������� ������� ��� ������ ������� �� ��������
	for (int i = 0; i < used.size(); ++i) // ����� �������� ��� ������� ������������� ��� ������� Bfs � ����������� ���-�� ����. �����
		used[i] = false;
	BfsRoutes(FromEdge); // Bfs � ����������� ���-�� ����. �����
	if (FromEdge == ToEdge) // c������, ��� ������ ���
		return 0;
	else
		return routes[ToEdge];
}

MyGraph::MyGraph(int a)
	: n(a), mas(a), depth(a, -1), routes(a, 0), used(n)
{}

void MyGraph::InputEdge(int v, int w)
{
	mas[v].push_back(w);
	mas[w].push_back(v);
}

int main()
{
	int n = 0, m = 0;
	scanf("%d", &n); // c������ ���-�� ������ � �����
	scanf("%d", &m); // c������ ���-�� ���� � �����
	MyGraph graph(n);
	for (int i = 0; i < m; ++i) // ���� �����
	{
		int v = 0, w = 0;
		scanf("%d", &v);
		scanf("%d", &w);
		graph.InputEdge(v, w);
	}
	int FromEdge = 0, ToEdge = 0;
	cin >> FromEdge; // ���� �� ����� ������� ������ ���������� ����
	cin >> ToEdge;// ���� � ����� ������� ������ ���������� ����
	cout << graph.result(FromEdge, ToEdge);
	return 0;
}