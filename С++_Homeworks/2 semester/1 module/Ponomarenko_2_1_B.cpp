/*��� ������������ ����������������� ����.
������� ���� ����������� �����.
��������� ��������� O(VE), + 1 ���� �� O(V + E).*/

#include <iostream>
#include <vector>
#include <queue>
using namespace std;

class MyGraph
{
private:
	int n; //���������� ������
	vector<vector<int>> mas; // �������� ����� ������� ���������
	vector<bool> used; // �������� �� �������
	queue<int> q; // ������� ��� Bfs
	vector<int> deapth; // ������� ��� ������ �������
	int min; // ������ ����� ������������ �����
public:
	MyGraph() : n(0), min(0) {};
	MyGraph(int a);
	~MyGraph() {};
	void Bfs(int v); // Bfs � ����������� ������� ��� ������ �������
	int result(); // ������� ��� ������ ����������
	void InputEdge(int v, int w); // ���� ����
};

void MyGraph::Bfs(int v)
{
	if (used[v]) // �������� ������� Bfs 
		return;
	q.push(v);
	used[v] = true;
	deapth[v] = 0;
	while (!q.empty())
	{
		v = q.front();
		q.pop();
		int ancestor = false; // ������ �� ������ (������) � ��������������� �������?
		for (int i = 0; i < mas[v].size(); ++i)
		{
			if (used[mas[v][i]] == false) // ������������ ������� ����� ���� ������ ������ ��������������� �������, ������� �� ������� ����� �� ���� ������, ��� ������� ��������������� �������
				deapth[mas[v][i]] = deapth[v] + 1;

			if (deapth[mas[v][i]] == deapth[v]) // ���� ������� ����� �� ����� ������, �� ��� ���� �������� ����, ����� �������� = deapth[v] * 2 + 1

			{
				if ((deapth[v] * 2 + 1) < min) // �������� �� �������������
					min = deapth[v] * 2 + 1;
			}
			if (((deapth[mas[v][i]] + 1) == deapth[v]) && (ancestor == true) && (mas[v][i] != -1)) // ���� ��� ������� ��� ������, �� ������ ����
			{
				if ((deapth[v] * 2) < min)  // �������� �� �������������
					min = deapth[v] * 2;
			}
			if (((deapth[mas[v][i]] + 1) == deapth[v]) && (ancestor == false) && (mas[v][i] != -1)) // ������ �� ������ � ��������������� �������?
				ancestor = true;

			if (used[mas[v][i]])
			{
				continue;
			};
			q.push(mas[v][i]);
			used[mas[v][i]] = true;
		}
	}
}

int MyGraph::result()
{
	for (int v = 0; v < n; ++v)
	{
		Bfs(v); // �������� Bfs �� ������ �������
		for (int j = 0; j < n; ++j) // ��� ������� ������ ������ Bfs �������� ������ used � ������ ��� �����
		{
			used[j] = false;
			deapth[j] = -1;
		}
		if (min == 3) // �����������. �� ������ ������ ������ 3
			break;
	}
	if (min == n + 1)
		return -1; // ���� �� ������ ����
	else
		return min;
	return 0;
}

MyGraph::MyGraph(int a)
	: n(a), mas(a), deapth(a, -1), used(n), min(a + 1)
{}

void MyGraph::InputEdge(int v, int w)
{
	mas[v].push_back(w);
	mas[w].push_back(v);
}

int main()
{
	int n = 0, m = 0;
	scanf("%d", &n); // c������ ���-�� ������ � �����
	scanf("%d", &m);// c������ ���-�� ���� � �����
	MyGraph graph(n);
	for (int i = 0; i < m; ++i) // ���� �����, ������ ������� ������� ���������
	{
		int v = 0, w = 0;
		scanf("%d", &v); // ������� ��������, ��� cin
		scanf("%d", &w); // ������� ��������, ��� cin
		graph.InputEdge(v, w);
	}
	printf("%d", graph.result()); // �������� ���������
	return 0;
}