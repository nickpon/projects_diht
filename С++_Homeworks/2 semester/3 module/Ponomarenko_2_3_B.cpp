#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include <algorithm>
using namespace std;

void FillMas(vector< vector<float> > &mas, vector<float> &X, vector<float> &Y, int n)
{
	for (int i = 0; i < n; ++i)
		for (int j = 0; j < n; ++j)
			mas[i][j] = pow(((X[i] - X[j]) * (X[i] - X[j]) + (Y[i] - Y[j]) * (Y[i] - Y[j])), 0.5);
}

void CreatePoints(int n, vector< vector<float> > &mas)
{
	vector<float> X(n), Y(n);
	for (int i = 0; i < n; ++i)
	{
		float x = (-2) * (rand() / (float)RAND_MAX) + 1;
		float y = (-2) * (rand() / (float)RAND_MAX) + 1;
		float s = x * x + y * y;
		float z0 = 0, z1 = 0;
		if ((s > 0) && (s <= 1))
		{
			z0 = 10 * x * pow(((-2) * log(s) / s), 0.5);
			z1 = 10 * y * pow(((-2) * log(s) / s), 0.5);
		}
		else
		{
			--i;
			continue;
		}
		bool flag = true;
		for (int j = 0; j < i; ++j)
		{
			if ((X[j] == z0) && (Y[j] == z1))
			{
				flag = false;
				break;
			}
		}
		if (flag == false)
			--i;
		else
		{
			X[i] = z0;
			Y[i] = z1;
		}
	}
	FillMas(mas, X, Y, n);
}

float CountWeight(int n, vector<int> &position, vector< vector<float> > &mas)
{
	float CW = 0;
	for (int i = 0; i < (n - 1); ++i)
	{
		CW += mas[position[i]][position[i + 1]];
	}
	return CW;
}

float CountMinWeight(int n, vector< vector<float> > &mas)
{
	vector<int> position(n);
	for (int i = 0; i < n; ++i)
		position[i] = i;
	float CW = 0;
	float CWmin = INFINITY;
	do 
	{
		CW = CountWeight(n, position, mas);
		if (CW < CWmin)
			CWmin = CW;
	} while (next_permutation(position.begin(), position.end()));
	return CWmin;
}

struct edge
{
	float b;
	float e;
	float w;
	edge(float b_, float e_, float w_) : b(b_), e(e_), w(w_) { };
	edge() { };
};

bool comprasion(const edge& a, const edge& b)
{
	return a.w < b.w;
}

int FromWhichSet(int x, vector<int> &set)
{
	if (x != set[x])
		set[x] = FromWhichSet(set[x], set);
	return set[x];
}

void UnionEdge(int x, int y, vector<int> &set, vector<int> &rank)
{
	x = FromWhichSet(x, set);
	y = FromWhichSet(y, set);
	if (rank[x] > rank[y])
		set[y] = x;
	else
		set[x] = y;
	if (rank[x] = rank[y])
		++rank[y];
}

void Kruskal(vector<edge> &e, vector<edge>&answer, vector<int> &set, vector <int> &rank, int n, int m)
{
	sort(e.begin(), e.end(), comprasion);
	for (int i = 0; i < m; ++i)
	{
		if (FromWhichSet(e[i].b, set) != FromWhichSet(e[i].e, set))
		{
			UnionEdge(e[i].b, e[i].e, set, rank);
			answer.push_back(e[i]);
		}
	}
}

float CountMSTWeight(int n, vector< vector<float> > &mas)
{
	int m = n * n;
	vector <edge> e(m);
	int counter = 0;
	for (int i = 0; i < n; ++i)
		for (int j = 0; (j < n); ++j)
		{
			e[counter].b = i;
			e[counter].e = j;
			e[counter].w = mas[i][j];
			++counter;
		}
	vector <int> set(n);
	vector <int> rank(n);
	for (int i = 0; i < n; ++i)
		set[i] = i;
	vector <edge> answer;
	Kruskal(e, answer, set, rank, n, m);

	vector<int> NumberOrder;
	vector<int> IsUsed(n);
	for (int i = 0; i < answer.size(); ++i)
	{
		if (!IsUsed[answer[i].b])
		{
			NumberOrder.push_back(answer[i].b);
			IsUsed[answer[i].b] = 1;
		}
		if (!IsUsed[answer[i].e])
		{
			NumberOrder.push_back(answer[i].e);
			IsUsed[answer[i].e] = 1;
		}
	}
	float sumMST = 0;
	for (int i = 0; i < NumberOrder.size() - 1; ++i)
		sumMST += mas[NumberOrder[i]][NumberOrder[i + 1]];
	return sumMST;
}

int main()
{
	int n = 0, repet = 0;
	cin >> n >> repet;
	vector<float> MinWeight(repet), MSTWeight(repet);
	for (int k = 0; k < repet; ++k)
	{
		vector< vector<float> > mas(n);
		for (int i = 0; i < n; ++i)
			mas[i].resize(n);
		CreatePoints(n, mas);
		MinWeight[k] = CountMinWeight(n, mas);
		MSTWeight[k] = CountMSTWeight(n, mas);
	}
	float AvgMin = 0, AvgMST = 0, StandDeviat = 0;
	for (int i = 0; i < repet; ++i)
	{
		AvgMin += MinWeight[i];
		AvgMST += MSTWeight[i];
		StandDeviat += pow((MSTWeight[i] - MinWeight[i]), 2);
	}
	AvgMin /= repet;
	AvgMST /= repet;
	StandDeviat =pow((StandDeviat / repet)/(n-1), 0.5);
	cout << AvgMin << " " << AvgMST << " " << StandDeviat << endl;
    return 0;
}