/*���������� �������� ����� ����� �������� ������ ����� � ��� ���������� ��������, � ������� ������� ���� ����� �� �����.
��������� ���������� �� ������� p �� ������ s ��� ��������� ���������� �������� �� p �� ���� �������� s, ������� ����� 
|p|. � ������ � � ������� ��������� ������� �����. ����� ������������ ������ ������� ���, ����� ���������� �� p �� 
s ���� ���������� ���������.*/

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

class MatrixGraph
{
private:
	vector < vector <int> > mas, revmas;
	vector <int> d, ptr, q, used;
	int N;
	int s;
	int t;
public:
	MatrixGraph() {};
	MatrixGraph(int N);
	void AddEdge(int from, int to, int weight);
	void FillInGraph(string s, string p, int QuestMarks, int QuestS);
	void print();
	void printrev();
	void printused();
	bool bfs();
	int dfs(int v, int flow);
	void dinic();
	void FindLeftEdges();
	void dfs2(int v);
	void InsertNumbers(string &SM);
};

void MatrixGraph::printrev()
{
	for (int i = 0; i < N; ++i)
	{
		for (int j = 0; j < N; ++j)
		{
			cout << revmas[i][j] << " ";
		}
		cout << endl;
	}
}

MatrixGraph::MatrixGraph(int N) : mas(N), N(N), revmas(N), s(0), t(N - 1), d(N), ptr(N), q(N), used(N)
{
	for (int i = 0; i < N; ++i)
	{
		mas[i].resize(N);
		revmas[i].resize(N);
	}
}

bool MatrixGraph::bfs()
{
	int qh = 0, qt = 0;
	q[qt++] = s;
	for (int i = 0; i < N; ++i)
		d[i] = -1;
	d[s] = 0;
	while (qh < qt) {
		int v = q[qh++];
		for (int to = 0; to<N; ++to)
			if (d[to] == -1 && revmas[v][to] < mas[v][to]) {
				q[qt++] = to;
				d[to] = d[v] + 1;
			}
	}
	return d[t] != -1;
}

int MatrixGraph::dfs(int v, int flow)
{
	if (!flow)  return 0;
	if (v == t)  return flow;
	for (int & to = ptr[v]; to<N; ++to) {
		if (d[to] != d[v] + 1)  continue;
		int pushed = dfs(to, min(flow, mas[v][to] - revmas[v][to]));
		if (pushed)
		{
			revmas[v][to] += pushed;
			revmas[to][v] -= pushed;
			return pushed;
		}
	}
	return 0;
}

void MatrixGraph::FindLeftEdges()
{
	for (int i = 0; i < N; ++i)
	{
		for (int j = 0; j < N; ++j)
		{
			if (revmas[i][j] < 0)
			{
				revmas[i][j] = 0;
			}
		}
	}
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			if (revmas[i][j] > 0)
				mas[j][i] -= revmas[i][j];
		}
	}
}

void MatrixGraph::dinic()
{
	int flow = 0;
	for (;;)
	{
		if (!bfs())  break;
		for (int i = 0; i < N; ++i)
			ptr[i] = 0;
		while (int pushed = dfs(s, 999999999))
			flow += pushed;
	}
	FindLeftEdges();
	dfs2(N - 1);
}

void MatrixGraph::dfs2(int v)
{
	used[v] = 1;
	for (int i = 0; i < N; ++i)
	{
		if ((!used[i]) && (mas[v][i] > 0))
			dfs2(i);
	}
}

void MatrixGraph::AddEdge(int from, int to, int weight)
{
	mas[from][to] += weight;
	mas[to][from] += weight;
}

void MatrixGraph::FillInGraph(string s, string p, int QuestMarks, int QuestS)
{
	int d = 1, h = 1, initiald = 1;
	for (int i = 0; i < (s.size() - p.size() + 1); ++i)
	{
		for (int j = 0; j < p.size(); ++j)
		{
			if ((s[i + j] == '0') && (p[j] == '?'))
			{
				AddEdge(0, QuestS + h, 1);
				++h;
			}
			if ((s[i + j] == '1') && (p[j] == '?'))
			{
				AddEdge(QuestMarks + 1, QuestS + h, 1);
				++h;
			}
			if ((s[i + j] == '?') && (p[j] == '0'))
			{
				AddEdge(0, d, 1);
				++d;
			}
			if ((s[i + j] == '?') && (p[j] == '1'))
			{
				AddEdge(QuestMarks + 1, d, 1);
				++d;
			}
			if ((s[i + j] == '?') && (p[j] == '?'))
			{
				AddEdge(d, QuestS + h, 1);
				++d;
				++h;
			}
		}
		if (s[i] == '?')
			++initiald;
		d = initiald;
		h = 1;
	}

}

void MatrixGraph::print()
{
	for (int i = 0; i < N; ++i)
	{
		for (int j = 0; j < N; ++j)
			cout << mas[i][j] << " ";
		cout << endl;
	}
}

void MatrixGraph::printused()
{
	for (int i = 0; i < N; ++i)
		cout << used[i] << " ";
	cout << endl;
}

void MatrixGraph::InsertNumbers(string &SM)
{
	int counter = 1;
	for (int i = 0; i < SM.size(); ++i)
	{
		if (SM[i] == '?')
		{
			if (!used[counter])
				SM[i] = '0';
			else
				SM[i] = '1';
			++counter;
		}
	}
}

int hem(string s, string p)
{
	int HemDist = 0;
	for (int i = 0; i < (s.size() - p.size() + 1); ++i)
	{
		for (int j = 0; j < p.size(); ++j)
		{
			if (s[i + j] != p[j])
				++HemDist;
		}
	}
	return HemDist;
}

int main()
{
	string s, p;
	cin >> s >> p;
	int sSize = s.size();
	int pSize = p.size();
	string SM = s + p;
	int SMSize = SM.size();
	int QuestS = 0;
	int QuestMarks = 0;
	for (int i = 0; i < s.size(); ++i)
	{
		if (s[i] == '?')
			++QuestS;
	}
	for (int i = 0; i < SM.size(); ++i)
	{
		if (SM[i] == '?')
			++QuestMarks;
	}
	MatrixGraph graph(QuestMarks + 2);
	graph.FillInGraph(s, p, QuestMarks, QuestS);
	graph.dinic();
	graph.InsertNumbers(SM);
	for (int i = 0; i < s.size(); ++i)
		s[i] = SM[i];
	for (int i = s.size(); i < (s.size() + p.size()); ++i)
		p[i - s.size()] = SM[i];
	cout << hem(s, p) << endl;
	cout << s << endl;
	cout << p << endl;
	return 0;
}