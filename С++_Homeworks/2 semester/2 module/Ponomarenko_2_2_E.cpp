/* ������ ��������������� ���������� ���� ����� �������� ���������.
��������� ������� ���������� ����� ����� ��� ���������.
�������������, ��� � ����� ��� ������ �������������� ����. */

#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
using namespace std;

void Floyd_Warshall(vector<vector<int>> &matrix, int N) // �������� ������-�������� 
{
	for (int k = 0; k < N; ++k)
		for (int i = 0; i < N; ++i)
			for (int j = 0; j < N; ++j)
				matrix[i][j] = min(matrix[i][j], matrix[i][k] + matrix[k][j]);
}

int main()
{
	ifstream fin("floyd.in");
	ofstream fout("floyd.out");
	int N = 0; // ���-�� ������ � �����
	fin >> N;
	vector <vector <int> > matrix(N);
	for (int i = 0; i < N; ++i) // ���� �������
	{
		matrix[i].resize(N);
		for (int j = 0; j < N; ++j)
			fin >> matrix[i][j];
	}
	Floyd_Warshall(matrix, N);
	for (int i = 0; i < N; ++i) // ����� ������� (����� ���� ������� ��-�� �������� � ��������� �� ����� �������)
	{
		for (int j = 0; j < N; ++j)
		{
			if (j != N - 1)
				fout << matrix[i][j] << ' ';
			else
				fout << matrix[i][j];
		}
		if (i != N - 1)
			fout << endl;
	}
	fin.close();
	fout.close();
	return 0;
}