/* ��������� �������� ����� �������� ������� ����� ��������.
��������� ����� ������ O((N + M)log N), ��� N � ���������� �������,
M � ��������� ����� ����� ����.

�������� ���� ���� ��������� � �������� �� �������� � 694 ������ (��������) 13.03.2017 */

#include <iostream>
#include <set>
#include <vector>
using namespace std;

const int INF = 0x7FFFFFFF; // ������������ int (��� ����������� �������������)

struct Edge
{
	int iTo;
	int weight;
};

class MyGraph
{
public:
	MyGraph() : nNodes(0) {};
	MyGraph(int _nNodes);
	int Dijkstra(int iSrc, int iDst); // �������� ��������
	void InputEdge(int iFrom, int iTo, int weight); // ���� ����
private:
	int nNodes; //���������� ������
	vector<vector<Edge>> graph;
};

MyGraph::MyGraph(int _nNodes)
	: nNodes(_nNodes), graph(_nNodes)
{}

void MyGraph::InputEdge(int iFrom, int iTo, int weight)
{
	Edge newEdge;
	newEdge.iTo = iTo;
	newEdge.weight = weight;
	graph[iFrom].push_back(newEdge);
	Edge newEdgeReverse;
	newEdgeReverse.iTo = iFrom;
	newEdgeReverse.weight = newEdge.weight;
	graph[newEdge.iTo].push_back(newEdgeReverse);
}

int MyGraph::Dijkstra(int iSrc, int iDst)
{
	vector <int> d(nNodes, INF); // �������, ��� ���� �� ������ ������� �� ��������� ����������
	d[iSrc] = 0; // ���� �� ��������� ������� �� ����� ���� ����� 0
	set <pair<int, int>> priority; // ������ - ���, ������ - ������ �������
	priority.insert(pair <int, int>(0, iSrc)); // ������� ������ �������

	while (!priority.empty()) // ���� �� ���������� ��� �������, � ������� ���� ���� �� ���������
	{
		int iCurFrom = priority.begin()->second; // ������ �������, �� ������� �� ������� ��� ����
		priority.erase(priority.begin()); // ������ �������, ��� ��� ��� ��� �����������
		for (Edge &curEdge : graph[iCurFrom]) // ���������� ���� ����� ���� ������� 
		{
			int iCurTo = curEdge.iTo;
			if (d[iCurTo] > d[iCurFrom] + curEdge.weight) // ���������� �� ����� ����������� ����?
			{											  // �� �� ����� ����� ��� �� 1 ������� �����, ������� ����� �� ������� used[]
				int oldWeight = d[iCurTo];
				d[iCurTo] = d[iCurFrom] + curEdge.weight; // ����������� ���� ����������� ����
				priority.erase({ oldWeight, iCurTo }); // ��������� ��������, ��� ��� ������ ������� ����
				priority.insert({ d[iCurTo], iCurTo });
			}
		}
	}
	return d[iDst]; // ������ ���������� ���� �� �������� ���������� �������
}


int main()
{
	int nNodes = 0, nEdges = 0;
	cin >> nNodes;
	cin >> nEdges;
	MyGraph graph(nNodes);
	for (int i = 0; i < nEdges; ++i)
	{
		int iFrom = 0, iTo = 0, weight = 0;
		cin >> iFrom;
		cin >> iTo;
		cin >> weight;
		graph.InputEdge(iFrom, iTo, weight);
	}
	int iSrc = 0, iDst = 0;
	cin >> iSrc; // �������, �� ������� ������ ���������� ����
	cin >> iDst; // �������, �� ������� ������ ���������� ����

	cout << graph.Dijkstra(iSrc, iDst);
	return 0;
}