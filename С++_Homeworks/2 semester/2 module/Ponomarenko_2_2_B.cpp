/* ���������� �������� ��������� ��������� ��� ������ ���������.
�������� - ��� �������� �� ������� ��������� ����� � ������� ���������� �� ��������� ��������� � �������������. ��������, ���� ��������� ����� �����:
GBP/USD: 0.67
RUB/GBP: 78.66
USD/RUB: 0.02
���� 1$ � �������� ���� USD->GBP->RUB->USD, ������� 1.054$. ����� ������� ��������� 5.4 */

#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

bool Ford_Bellman(vector < vector <double> > matrix, int N)
{
	vector <double> d(N, 0);
	d[0] = 1;
	bool any = false;

	for (int i = 0; i < (N - 1); ++i) // �� ������ �-� � e-maxx
	{
		for (int j = 0; j < N; ++j)
		{
			for (int k = 0; k < N; ++k)
				(d[k] < (d[j] * matrix[j][k])) ? d[k] = (d[j] * matrix[j][k]) : d[k] = d[k];
		}
	}

	for (int i = 0; i < N; ++i)
	{
		for (int j = 0; j < N; ++j)
		{
			if (d[j] < d[i] * matrix[i][j]) // ���� ��� ��� ���������� ����������, �� ������ ���� �������������� ����
				return true;
		}
	}
	return false;
}

int main()
{
	int N = 0; // ���-�� ������ � �����
	cin >> N;
	vector <vector <double> > matrix(N);
	for (int i = 0; i < N; ++i) // ���� �������
	{
		matrix[i].resize(N, 0);
		for (int j = 0; (j < N); ++j)
		{
			if (i == j)
				continue;
			cin >> matrix[i][j];
		}
	}
	Ford_Bellman(matrix, N) ? cout << "YES" : cout << "NO";
	return 0;
}