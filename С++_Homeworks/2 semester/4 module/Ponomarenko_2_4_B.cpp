/* � ���������-������������ ���� ������� ������������� ������.
�� ���� ��� �� ������������� ��������� ������� ������� � ��������� ����.
����� ������� �������������� ���������� ��������� �������� ������ ������
����� ������������� �������� ��� ����� ������������ ���������.
��� ����������� �������������� ��������� ����� ������� ������ �������
����� �� ��������� ���������� �������. �������� ��������� ��� �����������
������� ������ ������� ����� �� �������. ��������� �������� ����������� ������� - O(log N).
���� �������� ����� ������� R, G � B (0 ? R, G, B ? 255),
��� ������� = R + G + B. ���� (R1, G1, B1) ������ ����� (R2, G2, B2),
���� R1 + G1 + B1 < R2 + G2 + B2. */

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class SegmentTree
{
private:
	vector <int> tree; // ������, � ������� �������� ������
	vector <int> arr; // ����������� ������� ������
	int N, K, nMax; // nMax - ������������ ������� ������ �� ������� ��������� �������

public:
	SegmentTree() { };
	SegmentTree(int _n, int _k, vector <int> &_arr);
	int CountnMax(int _n); // ������� ������������ ������� ������ �� ������� ��������� �������
	void BuildTree(); // ������ �������� ������, ����� ��� � �������
	void PrintTree(); // ������� ��� ������ ������
	int FillnMax(); // ������ � nMax �� Main()
	int Query(int v, int leftPosition, int rightPosition, int l, int r); // ������ �� ��������
	void NewUpdate(int v, int leftPosition, int rightPosition, int l, int r, int val); // ���������� �� ���������
};

SegmentTree::SegmentTree(int _n, int _k, vector <int> &_arr) : N(_n), arr(_n)
{
	arr = _arr;
	nMax = CountnMax(_n);
	tree.resize(2 * nMax, 100000); // ��������� �����-������ ������� ���������
}

int SegmentTree::CountnMax(int _n) // ��������� ������ ������������ �������
{
	nMax = 1;
	while (nMax<_n)
		nMax <<= 1;
	return nMax;
}

void SegmentTree::BuildTree() // ������ �������� ������, ���������� � �������
{
	for (int i = 0; i < N; ++i) // ��������� ������� - ���� ��� ��������
		tree[nMax + i] = arr[i];
	for (int i = nMax - 1; i > 0; --i) // ��������� ������, ������ �� ���������� ������
		tree[i] = min(tree[2 * i], tree[2 * i + 1]);
}

void SegmentTree::PrintTree()
{
	cout << "nMax " << nMax << endl;
	cout << "____________" << endl;
	for (int i = 0; i < 2 * nMax; ++i)
		cout << tree[i] << " ";
	cout << endl;
	cout << "____________" << endl;
}

int SegmentTree::FillnMax() // ������ � privat-�����
{
	return nMax;
}

void SegmentTree::NewUpdate(int v, int leftPosition, int rightPosition, int l, int r, int val) // ���������� �� ���������
{
	l += nMax;
	r += nMax;
	for (int i = l; i <= r; ++i) // ������� �������� ������ ������� ������ � ����������� �� �������
		tree[i] = val;
	int new_l = l / 2, new_r = r / 2;
	while (new_l > 0) // ������ ��������� �������� �� ���� ������� ����, ������������ �� ������� ����
	{				  // ��������� ��� �������� �� ��� ���, ���� �� ����� � �������
		for (int i = new_l; i <= new_r; ++i)
			tree[i] = min(tree[2 * i], tree[2 * i + 1]);
		new_l /= 2;
		new_r /= 2;
	}
}

int SegmentTree::Query(int v, int leftPosition, int rightPosition, int l, int r) // ������ �� ���������
{
	if (r<l) return 100000; // ������, ������� ������� �����
	if (l == leftPosition && r == rightPosition)
		return tree[v]; // ������ ���, ��� �����. ������� ����� � ���� ����
	int middle = (leftPosition + rightPosition) / 2;
	return min(Query(v * 2, leftPosition, middle, l, min(middle, r)), Query(v * 2 + 1, middle + 1, rightPosition, max(l, middle + 1), r)); // ��� ������, ����� �� ������
																																		   // ������ � ����� � ������ �����, ���������� ������� ��� �� ������
}

int main()
{
	int N = 0, K = 0, C = 0, D = 0, R = 0, G = 0, B = 0, E = 0, F = 0;
	cin >> N;
	vector <int> arr(N);
	for (int i = 0; i < N; ++i)
	{
		cin >> R >> G >> B;
		arr[i] = R + G + B;
	}
	cin >> K;
	SegmentTree tree(N, K, arr);
	tree.BuildTree();
	int nMax = tree.FillnMax();
	vector <vector <int> > changer(K);
	for (int i = 0; i < K; ++i)
	{
		changer[i].resize(6);
		cin >> C >> D >> R >> G >> B >> E >> F;
		changer[i][1] = C;
		changer[i][2] = D;
		changer[i][3] = R + G + B;
		changer[i][4] = E;
		changer[i][5] = F;
	}
	for (int i = 0; i < K; ++i)
	{
		tree.NewUpdate(1, 0, nMax - 1, changer[i][1], changer[i][2], changer[i][3]);
		cout << tree.Query(1, 0, nMax - 1, changer[i][4], changer[i][5]) << endl;
	}
	return 0;
}