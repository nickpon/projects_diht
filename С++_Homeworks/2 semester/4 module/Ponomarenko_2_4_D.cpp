﻿/*Реализуйте структуру данных “массив строк” на основе
декартового дерева по неявному ключу со следующими методами:*/

#include <iostream>
#include <vector>
#include <string>

using namespace std;

struct Node // структура для хранения атрибутов узда дерева
{
	int y;   // номер
	int sizeofundertree;  // размер поддерева
	string val; // строчка, которую храним в узле
	Node *l, *r; // левый и правый сыновья
};

class MyTree
{
private:
	Node* tree;
	int number; // для поочерёдной реализации приоритетов
public:
	MyTree() : tree(nullptr), number(0) {}; // дерева нет, поэтому изначальный приоритет и равен 0
	Node* get_node(); // функция для доступа к privat-члену при рекурсивном вызове, например, функции GetAt
	Node* new_node(string val); // создание одноэлементного дерева. Его хорохо использовать при слиянии в процессе вставки элемента в исходное дерево
	void newsize(Node *t); // обновление информации об размере поддерева
	Node* merge(Node* t1, Node *t2); // слияния деревьев
	void split(Node *t, int x, Node *&tleft, Node *&tright); // разделение деревьев
	string GetAtDirect(Node* t, int position); // из-за задания и рекурсии раздваиваем функцию
	void InsertAt(int position, const string& value);
	void DeleteAt(int position);
	string GetAt(int position);
};

string MyTree::GetAt(int position)
{
	return GetAtDirect(tree, position);
}

Node* MyTree::get_node()
{
	return tree; // выводим privat-член класса. Используется в дальнейшем для рекурентного вызова
}

Node* MyTree::new_node(string val)
{
	Node *result = new Node;
	result->y = number; // инициализирумем приоритет
	++number; // увеличиваем приоритет на один, чтобы у нас не было коллизий
	result->sizeofundertree = 1; // считаем, что дерево из одной вершины имеет размер поддерева 1
	result->val = val;
	result->l = result->r = nullptr; // нет детей
	return result;
}

void MyTree::newsize(Node *t)
{
	if (t == nullptr)  // если нет дерева
		return;
	int idxl = 0; // размер левого поддерева
	if (t->l == nullptr)
		idxl = 0;
	else
		idxl = t->l->sizeofundertree;
	int idxr = 0; // размер правого поддерева
	if (t->r == nullptr)
		idxr = 0;
	else
		idxr = t->r->sizeofundertree;
	t->sizeofundertree = 1 + idxl + idxr; // общий размер поддерева = размер левого поддерева + размер правого поддерева + сам корень
}

Node* MyTree::merge(Node* tleft, Node *tright) // слияние
{
	if (!tleft) // cлучаи, когда сливаем с ничем (пустым деревом)
		return tright;
	if (!tright)
		return tleft;
	if (tleft->y > tright->y)
	{
		tleft->r = merge(tleft->r, tright);
		newsize(tleft);
		return tleft;
	}
	else
	{
		tright->l = merge(tleft, tright->l);
		newsize(tright);
		return tright;
	}
}

void MyTree::split(Node *t, int x, Node *&tleft, Node *&tright) // разделение
{																// взял из первого семестра + добавил поддержку размеров
	if (!t)
	{
		tleft = tright = nullptr;
		return;
	}
	int idxl = 0; // размер левого поддерева
	if (t->l == nullptr)
		idxl = 0;
	else
		idxl = t->l->sizeofundertree;
	if (idxl < x)
	{
		split(t->r, x - idxl - 1, t->r, tright);
		tleft = t;
	}
	else
	{
		split(t->l, x, tleft, t->l);
		tright = t;
	}
	newsize(t); // обновляем размер
}

void MyTree::InsertAt(int position, const string& value)
{
	Node *tleft, *tright, *t;
	Node* newone = new_node(value); // создаём дерево из одного элемента
	split(tree, position, tleft, tright); // разделяем по позиции
	t = merge(tleft, newone); // сливаем с левым поддеревом
	tree = merge(t, tright); // результат сливаем с правым поддеревом
}

void MyTree::DeleteAt(int position)
{
	Node *tleft, *tright, *t;
	split(tree, position, tleft, tright); // разрезаем массив по индексу position
	split(tright, 1, t, tright); // Правый результат разрезаем по индексу 1. Получаем одноэлементное дерево с корнем position
	tree = merge(tleft, tright); // сливаем оставшиеся деревья без этого одноэлементного
}

string MyTree::GetAtDirect(Node* t, int position)
{
	int idxl = 0; // размер левого поддерева
	if (t->l == nullptr)
		idxl = 0;
	else
		idxl = t->l->sizeofundertree;
	if (position < idxl) // мы можем либо не попасть слева, либо попасть, либо не попасть справа в position. Обрабатываем эти случаи
		return GetAtDirect(t->l, position);
	else if (position == idxl)
		return t->val;
	else
		return GetAtDirect(t->r, position - idxl - 1);
}

int main()
{
	int k = 0;
	cin >> k;
	MyTree tree;
	for (int i = 0; i < k; ++i)
	{
		char symbol = ' ';
		int position = 0;
		cin >> symbol;
		if (symbol == '+')
		{
			string str = "";
			cin >> position >> str;
			tree.InsertAt(position, str);
		}
		if (symbol == '?')
		{
			cin >> position;
			cout << tree.GetAt(position) << endl;
		}
		if (symbol == '-')
		{
			cin >> position;
			tree.DeleteAt(position);
		}
	}
}