﻿/*Задано дерево с корнем, содержащее (1 ≤ n ≤ 100 000) вершин, пронумерованных от 0 до n-1.
Требуется ответить на m (1 ≤ m ≤ 10 000 000) запросов о наименьшем общем предке для пары вершин.
Запросы генерируются следующим образом. Заданы числа a1, a2 и числа x, y и z.
Числа a3, ..., a2m генерируются следующим образом: ai = (x ai-2 + y ai-1 + z) mod n.
Первый запрос имеет вид (a1, a2). Если ответ на i-1-й запрос равен v, то i-й запрос имеет вид ((a2i-1 + v) mod n, a2i).
Для решения задачи можно использовать метод двоичного подъёма.


Основной класс и функция BFS взяты из первой задачи первого модуля.*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
#include <queue>
using namespace std;

class MyGraph
{
private:
	long long n; //количество вершин
	long long l; // макс нужная степень двойки
	int timer; // таймер
	vector <vector <int> > dp; // мастрица достижимости
	vector<vector<long long>> mas; // хранение графа списком смежности
	queue<long long> q; // очередь для Bfs
	vector <bool> used; // для BFS
	vector<long long> deapth; // глубина для каждой вершины
	long long min; // храним длину минимального цикла
	vector <long long> p; // родитель
	vector<int> tin; // время входа
	vector<int> tout; // время выхода
public:
	MyGraph() : n(0), min(0) {};
	MyGraph(long long a, vector <long long> &_p);
	~MyGraph() {};
	void Bfs(long long v); // Bfs с вычислением глубины для каждой вершины
	void InputEdge(long long v, long long w); // ввод рёбер
	void Filldp();
	void dfs(int v, int p);
	int lca(int a, int b);
};

void MyGraph::dfs(int v, int p) 
{
	tin[v] = ++timer;
	for (size_t i = 0; i < mas[v].size(); ++i)
	{
		int to = mas[v][i];
		if (to != p)
			dfs(to, v);
	}
	tout[v] = ++timer;
}

int MyGraph::lca(int v, int u)
{
	if (tin[v] <= tin[u] && tout[v] >= tout[u])  return v;
	if (tin[u] <= tin[v] && tout[u] >= tout[v])  return u;
	for (int i = l; i >= 0; --i)
		if (!(tin[dp[v][i]] <= tin[u] && tout[dp[v][i]] >= tout[u]))
			v = dp[v][i];
	return dp[v][0];
}

void MyGraph::Bfs(long long v)
{
	if (used[v]) // типичный алгорим Bfs 
		return;
	q.push(v);
	used[v] = true;
	deapth[v] = 0;
	while (!q.empty())
	{
		v = q.front();
		q.pop();
		for (long long i = 0; i < mas[v].size(); ++i)
		{
			if (used[mas[v][i]])
				continue;
			deapth[mas[v][i]] = deapth[v] + 1;
			q.push(mas[v][i]);
			used[mas[v][i]] = true;
		}
	}
}

MyGraph::MyGraph(long long a, vector <long long> &_p)
	: n(a), mas(a), deapth(a, -1), used(n), min(a + 1), p(n), dp(n), tin(n), tout(n), timer(0)
{
	p = _p;
	l = 1;
	while ((1 << l) <= n)  ++l;
	for (long long i = 0; i < n; ++i)
		dp[i].resize(l + 1);
}

void MyGraph::InputEdge(long long v, long long w)
{
	mas[v].push_back(w);
	mas[w].push_back(v);
}

void MyGraph::Filldp()
{
	for (long long i = 1; i < n; ++i)
		dp[i][0] = p[i];
	for (long long j = 1; j <= l; ++j)
		for (long long i = 1; i < n; ++i)
			dp[i][j] = dp[dp[i][j - 1]][j - 1];
}

int main()
{
	long long n = 0;
	long long m = 0;
	cin >> n >> m;
	vector <long long> p(n);
	for (long long i = 1; i < n; ++i)
		cin >> p[i];
	MyGraph graph(n, p);
	for (long long i = 1; i < n; ++i)
		graph.InputEdge(i, p[i]);
	graph.Bfs(0);
	graph.dfs(0, 0);
	graph.Filldp();
	long long a1 = 0, a2 = 0, a3 = 0, a4 = 0;
	cin >> a1 >> a2;
	long long x = 0, y = 0, z = 0;
	cin >> x >> y >> z;
	long long sum = 0;
	long long v = 0;
	v = graph.lca(a1, a2);
	sum += v;
	for (long long i = 2; i <= m; ++i)
	{
		long long temp = 0;
		a3 = (x * a1 + y * a2 + z) % n;
		a4 = (x * a2 + y * a3 + z) % n;
		temp = graph.lca((a3 + v) % n, a4);
		sum += temp;
		v = temp;
		a1 = a3;
		a2 = a4;
	}
	cout << sum << endl;
    return 0;
}