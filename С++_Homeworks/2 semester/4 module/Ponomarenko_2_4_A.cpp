/*���� ����� N � ������������������ �� N ����� �����.
����� ������ ���������� ���������� �� �������� ����������.
��� ������� ������ ����������� ��������� ������ Sparse Table.
��������� ����� ��������� ������� ��������� O(1).
����� ���������� ��������� ������ O(n log n).*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <set>

using namespace std;

int Compare(int a, int b, int c, int d) // it's better to use td::set for sort because we shouldn't think about correlations,
{										// e.g. in array 1,1,3,4 we need 3, not 1
	set <int> s;
	s.insert(a);
	s.insert(b);
	s.insert(c);
	s.insert(d);
	return *(++s.begin());
}

void Create1SaprseTable(vector <int> &arr, int N, vector < vector <int> > &SparseTable1)
{
	for (int i = 0; i < N; ++i)
		SparseTable1[0][i] = arr[i];
	int power = 0;
	int SizeOfUpperLine = N;
	int i = 1;
	int j = 0;
	while (SizeOfUpperLine > pow(2, power))
	{
		while (j + pow(2, power) < SizeOfUpperLine)
		{
			SparseTable1[i][j] = min(SparseTable1[i - 1][j], SparseTable1[i - 1][j + pow(2, power)]);
			++j;
		}
		++i;
		j = 0;
		SizeOfUpperLine -= pow(2, power);
		++power;
	}
}

int len(int length) // finds the nearet power of 2 that the number pow(2,n) < length
{					// and is max from all varients
	if (length == 1)
		return 0;
	else
		return len(length / 2) + 1;
}

void CreateSparseTable2(vector <int> &arr, int N, vector < vector <int> > &SparseTable1, vector < vector <int> > &SparseTable2)
{
	for (int i = 0; i < N; ++i)		// first line is unchanged array
		SparseTable2[0][i] = arr[i];

	for (int i = 0; i < N - 1; ++i)	// put the least from each pair to the corresponding position
		SparseTable2[1][i] = max(SparseTable2[0][i], SparseTable2[0][i + 1]);

	int power = 1;
	int SizeOfUpperLine = N - 1;
	int i = 2;
	int j = 0;
	while (SizeOfUpperLine > pow(2, power))	// put the second element from the corresponding interval, divisible by 2
	{
		while (j + pow(2, power) < SizeOfUpperLine)
		{
			SparseTable2[i][j] = Compare(SparseTable1[i - 1][j], SparseTable1[i - 1][j + pow(2, power)], SparseTable2[i - 1][j], SparseTable2[i - 1][j + pow(2, power)]);
			++j;
		}
		++i;
		j = 0;
		SizeOfUpperLine -= pow(2, power);
		++power;
	}
}

int main()
{
	int N = 0, M = 0;
	cin >> N >> M;

	vector <int> arr(N);
	for (int i = 0; i < N; ++i)
		cin >> arr[i];

	vector <int> beg(M), end(M);
	for (int i = 0; i < M; ++i)
		cin >> beg[i] >> end[i];

	vector < vector <int> > SparseTable1(log(N) / log(2) + 1);
	for (int i = 0; i < log(N) / log(2); ++i)
		SparseTable1[i].resize(N);

	Create1SaprseTable(arr, N, SparseTable1); // create the first Sparse Table for the the first ordinal statistics

	vector < vector <int> > SparseTable2(log(N) / log(2) + 1);
	for (int i = 0; i < log(N) / log(2); ++i)
		SparseTable2[i].resize(N);

	CreateSparseTable2(arr, N, SparseTable1, SparseTable2); // see more in definition

	for (int i = 0; i < M; ++i) // finds the needed number for each request
	{
		int l = beg[i] - 1, r = end[i] - 1;
		int length = len(r - l);
		if (length == 0)
			cout << max(SparseTable1[length][l], SparseTable1[length][r - pow(2, length) + 1]) << endl;
		else
			cout << Compare(SparseTable1[length][l], SparseTable1[length][r - pow(2, length) + 1], SparseTable2[length][l], SparseTable2[length][r - pow(2, length) + 1]) << endl;
	}
	return 0;
}