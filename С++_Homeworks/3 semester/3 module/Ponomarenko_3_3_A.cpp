﻿/*Даны два отрезка в пространстве (x1, y1, z1) - (x2, y2, z2) и
(x3, y3, z3) - (x4, y4, z4). Найдите расстояние между отрезками.*/

#include <iostream>
#include <iomanip>
#include <cmath>

using std::cin;
using std::cout;

struct Point {
	double x;
	double y;
	double z;
};

// Sum of two vectors
Point sum(Point a, Point b) {
	Point ans;
	ans.x = (a.x + b.x);
	ans.y = (a.y + b.y);
	ans.z = (a.z + b.z);
	return ans;
}

// Divides the vector by the number
Point div(Point a, int num) {
	a.x /= num;
	a.y /= num;
	a.z /= num;
	return a;
}

// Multiplies the vector and the number
Point mult(Point a, int num) {
	a.x *= num;
	a.y *= num;
	a.z *= num;
	return a;
}

// Scalar difference between two vectors
double comp(Point a, Point b) {
	return sqrt((double)((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y) + (a.z - b.z) * (a.z - b.z)));
}

// Count the distance between the segment given and the point
double dist(Point a, Point b, Point m) {
	double EPS = 0.0000000001; // accuracy factor
	double t = 0.0; // par. if the segment equation
	double l2 = comp(a, b);

	if (l2 - EPS < 0) // if segment is just a point
		return comp(a, m);

	// avoid division by 0
	if ((((b.x - a.x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y) + (b.z - a.x) * (b.z - a.x)) - EPS) < 0)
		t = 0;
	else
		t = ((m.x - a.x) * (b.x - a.x) + (m.y - a.y) * (b.y - a.y) + (m.z - a.z) * (b.z - a.z)) /
		((b.x - a.x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y) + (b.z - a.z) * (b.z - a.z));

	// according to the segment equation, t must be from 0 to 1
	// if not, use endpoints of the segment instead
	if ((t - EPS < 0) || (t + EPS < 0))
		t = 0;
	if ((t + EPS > 1) || (t - EPS > 1))
		t = 1;

	return sqrt((double)((a.x - m.x + (b.x - a.x) * t) * (a.x - m.x + (b.x - a.x) * t) + (a.y - m.y + (b.y - a.y) * t) *
		(a.y - m.y + (b.y - a.y) * t) + (a.z - m.z + (b.z - a.z) * t) * (a.z - m.z + (b.z - a.z) * t)));
}

// Triple search. We are permitted to use this method because
// the distance function has only one local minimum on the segment.
// rec stands to the depth of the recursion. Actually, it help us to
// increase the accuracy of calculations
double ter_search(Point a, Point b, Point c, Point d, int rec) {
	Point l = a;
	Point r = b;

	--rec;

	if (rec == 0) // the end of the recursion
		return dist(c, d, div(sum(r, l), 2));

	Point m1 = div(sum(mult(l, 2), r), 3); // 1/3 of the segment considered
	Point m2 = div(sum(mult(r, 2), l), 3); // 2/3 of the segment considered

	if (dist(c, d, m1) > dist(c, d, m2))
		return ter_search(m1, b, c, d, rec);
	else
		return ter_search(a, m2, c, d, rec);
}

int main() {
	Point a, b, c, d; // initial points
	cin >> a.x >> a.y >> a.z;
	cin >> b.x >> b.y >> b.z;
	cin >> c.x >> c.y >> c.z;
	cin >> d.x >> d.y >> d.z;

	std::cout << std::fixed << std::showpoint;
	std::cout << std::setprecision(8);
	cout << ter_search(a, b, c, d, 800); // 800 is the depth of the recursion

	return 0;
}