﻿/*Даны два выпуклых многоугольника на плоскости.
В первом n точек, во втором m. Определите, пересекаются ли они за O(n + m).
Указание. Используйте сумму Минковского.*/

#include <iostream>
#include <vector>
#include <algorithm>

using std::cin;
using std::cout;
using std::vector;
using std::sort;
using std::swap;

// 2d point structure for vert. of polygons 
struct Point {
	double x;
	double y;
};

// cross product
bool comp(const Point &a, const Point& b) {
	return ((a.x * b.y - a.y * b.x) > 0);
}

class Minkowski {
private:
	double E; // accuracy coefficient
	vector<Point> P; // first polygon 
	vector<Point> Q; // second polygon
	int n; // size of polygon P
	int m; // size of polygon Q
public:
	Minkowski(); // default constructor
	Minkowski(const vector<Point> &_P, const vector<Point> &_Q, const int _n, const int _m);
	int Angle(const Point &a, const Point &b, const Point &c, const Point &d) const;
	vector<Point> Minkowski_sum();
	vector <Point> Route(vector <Point> &points, int n);
	bool ZeroPointIsInside(const vector<Point> &P, int n);
	bool answer(); // function to solve the problem
};

Minkowski::Minkowski() : E(0.000001), n(0), m(0), P(0), Q(0) { };

Minkowski::Minkowski(const vector<Point> &_P, const vector<Point> &_Q, const int _n, const int _m) :
	E(0.000001), P(_P), Q(_Q), n(_n), m(_m) { };

// Сounts the polar angle between vectors vec1 = (b - a) and vec2 = (d - c).
// If vec2 if greater, returns 0.
// If vec1 is greater, returns 1.
// If vec1 is equal to vec1, returns 2.
inline int Minkowski::Angle(const Point &a, const Point &b, const Point &c, const Point &d) const {
	Point vec1; // define vec1
	vec1.x = b.x - a.x;
	vec1.y = b.y - a.y;

	Point vec2; // define vec2
	vec2.x = d.x - c.x;
	vec2.y = d.y - c.y;

	double par1 = (vec1.y / vec1.x); // tg of vec1
	double par2 = (vec2.y / vec2.x); // tg of vec2

									 // If signs of x-coordinates of vec1 and vec2 are different, 
									 // return the corresponding answer.
	if ((vec1.x < 0) && (vec2.x > 0))
		return 0;
	if ((vec1.x > 0) && (vec2.x < 0))
		return 1;

	// if they are equal
	if (fabs(par1 - par2) < E)
		return 2;

	// if polar angle of vec1 is (pi / 2) or (3 * pi / 2)
	if (fabs(vec1.x) < E) {
		// if polar angle if (pi / 2)
		if (vec1.y > 0) {
			// vec2 is in the third of fourth coordinate quarter
			if (vec2.x < 0)
				return 1;
			else
				return 0;
		}
		else
			return 0;
	}

	// same about vec2
	if (fabs(vec2.x) < E) {
		if (vec2.y > 0) {
			if (vec1.x > 0)
				return 1;
			else
				return 0;
		}
		else {
			// compare angles
			if (fabs(par1 - par2) > E)
				return 1;
			else
				return 0;
		}
	}

	// simple case, when angles are not equal to (pi / 2) or (3 * pi / 2)
	if (par1 < par2)
		return 1;
	else
		return 0;
}

bool Minkowski::ZeroPointIsInside(const vector<Point> &P, int points) {
	std::vector<double> buffer;

	//case for points
	if (points == 0)
		return false;

	// find all intersections with OX of edges of polygon
	for (int i = 0; i < (points - 1); ++i) {
		if (((P[i].y < 0) && (P[i + 1].y > 0)) || ((P[i].y > 0) && (P[i + 1].y < 0))) {
			// edge intersect Ox. To find out z.x, where c - intersection point, we'd better 
			// remember line equation y = k * x + h. Substituting the points for the variables,
			// we find values of k and h. Substituting k and h for the original equation,
			// and adjusting y to zero, we obtain the desired z.x.
			double z = P[i].x - P[i].y * ((P[i + 1].x - P[i].x) / (P[i + 1].y - P[i].y));
			buffer.push_back(z);
		}
		// point lies on Ox
		if (fabs(P[i].y) < E)
			buffer.push_back(P[i].x);
	}

	// last point to check
	if (fabs(P[points - 1].y) < E)
		buffer.push_back(P[points - 1].x);

	// if polygon do not intersect with Ox,
	// zero point is not inside
	if (buffer.empty())
		return false;

	// find max and min values of points on Ox
	double min_x = buffer[0];
	double max_x = buffer[0];
	for (int i = 0; i < buffer.size(); ++i) {
		if (buffer[i] < min_x)
			min_x = buffer[i];
		if (buffer[i] > max_x)
			max_x = buffer[i];
		// zero point is on the border, it belongs to the polygon
		if ((fabs(max_x) < E) || (fabs(min_x) < E))
			return true;
	}

	// if they have different signs, zero point is truely inside polygon given
	return (((min_x < 0) && (max_x > 0)) || ((max_x > 0) && (min_x < 0)));
}

vector<Point> Minkowski::Minkowski_sum() {
	// array, where hull is kept
	vector<Point> answer;

	// case for the point
	int i = 0, j = 0;
	if ((m == 1) && (n == 1)) {
		Q[0].x *= (-1);
		Q[0].y *= (-1);
		if ((P[0].x == Q[0].x) && (P[0].y == Q[0].y))
			answer.push_back(P[0]);
		return answer;
	}

	// until the beginning is reached 
	while ((i < (n)) && (j < (m))) {
		// push this point to the hull
		Point r;
		r.x = P[i].x + Q[j].x;
		r.y = P[i].y + Q[j].y;
		answer.push_back(r);

		// compare polar angles of vec1 = (P[i + 1] - P[i]) and
		// vec2 = (Q[i + 1] - Q[i])
		int param = Angle(P[i], P[i + 1], Q[j], Q[j + 1]);
		if (param == 0) // angle(vec1) < angle(vec2)
			++j;
		if (param == 1) // angle(vec1) > angle(vec2)
			++i;
		if (param == 2) { // angle(vec1) = angle(vec2)
			++i;
			++j;
		}
	}

	// Here, cases will be consider when one of the iterators reached the end,
	// while the second did not. The hull must be closed, so all remaining points
	// must be added to it. They can be found as the sum of the starting point
	// to which one of the iterators reached, and the points of another polygon
	// not yet considered.
	if ((i == n) && (j == m)) {
		Point w;
		w.x = P[n].x + Q[m].x;
		w.y = P[n].y + Q[m].y;
		return answer;
	}
	if (i == n) {
		for (int t = j; t <= m; ++t) {
			Point w;
			w.x = P[n].x + Q[t].x;
			w.y = P[n].y + Q[t].y;
			answer.push_back(w);
		}
		return answer;
	}
	if (j == m) {
		for (int t = i; t <= n; ++t) {
			Point w;
			w.x = Q[m].x + P[t].x;
			w.y = Q[m].y + P[t].y;
			answer.push_back(w);
		}
		return answer;
	}
}

vector <Point> Minkowski::Route(vector <Point> &points, int n) {
	// Find the minimum point corresponding to the condition described below.
	// This point will be initial in our route.
	// Put int in the beggining of our array.
	for (int i = 1; i < n; ++i) {
		if ((points[i].x < points[0].x) || (((points[i].x - points[0].x) < E) && (points[i].y < points[0].y)))
			swap(points[i], points[0]);
	}

	// Sort all points of the polygon corresponding to the minimum
	// point found above. Let's move the given point to the center
	// of coordinates in order to calculate correctly.
	double x = points[0].x;
	double y = points[0].y;
	for (int i = 0; i < n; ++i) {
		points[i].x -= x;
		points[i].y -= y;
	}

	// Sort points using cross product of thier position vectors
	sort(points.begin() + 1, points.end(), comp);

	// return the polygon to its initial position 
	for (int i = 0; i < n; ++i) {
		points[i].x += x;
		points[i].y += y;
	}

	return points;
}

bool Minkowski::answer() {
	// map the second polygon with respect to the center coordinates
	for (int i = 0; i < m; ++i) {
		Q[i].x *= (-1);
		Q[i].y *= (-1);
	}

	// rearrange the vertices in both polygons in the "right" order
	P = Route(P, n);
	Q = Route(Q, m);

	// as in the algo presented in class, make copies
	// of the first elements of each polygon,
	// push them back to corresponding polygon
	P.push_back(P[0]);
	P.push_back(P[1]);
	Q.push_back(Q[0]);
	Q.push_back(Q[1]);

	// ans contains the array of points which stand for Minkowski sum hull
	vector <Point> ans = Minkowski_sum();

	// Is zero point(the center coordinates) is inside?
	// If yes, it means that there are two vectors from P and inverted Q,
	// whose vector sum is equal to null vector. It means that there is 
	// an intersection between these two polygons.
	if (ZeroPointIsInside(ans, ans.size()))
		return true;
	else
		return false;
}

int main()
{
	int n = 0; // the size of the first polygon to enter (initial size of P)
	int m = 0; // the size of the second polygon to enter (initial size of Q)
	cin >> n;

	vector <Point> P(n); // enter Polygon P
	for (int i = 0; i < n; ++i)
		cin >> P[i].x >> P[i].y;

	cin >> m;

	vector <Point> Q(m); // enter polygon Q
	for (int i = 0; i < m; ++i)
		cin >> Q[i].x >> Q[i].y;

	// create object of the class Minkowski,
	// where all work will be done
	Minkowski MyClass(P, Q, n, m);

	if (MyClass.answer())
		cout << "YES";
	else
		cout << "NO";

	return 0;
}