﻿/*В этой задаче разрешается подключать <iostream>, <vector> и <string> и только их.

Напишите класс BigInteger для работы с длинными целыми числами. Должны поддерживаться операции:

сложение, вычитание, умножение, деление, остаток по модулю, работающие так же, как и для int;
составное присваивание с этими операциями;
унарный минус, префиксный и постфиксный инкремент и декремент,
операторы сравнения == != < > <= >=
вывод в поток и ввод из потока;
метод toString(), возвращающий строковое представление числа;
конструирование из int (в том числе неявное преобразование, когда это надо);
неявное преобразование в bool, когда это надо (должно работать в условных выражениях).
Асимптотические ограничения на время работы операторов в контесте не проверяются,
но реализация должна работать за:

Сложение, вычитание, унарный минус, операторы сравнения – O(n),
Умножение, деление, остаток по модулю – O(n2),
где n – количество разрядов большего числа (по модулю).
В вашем файле должна отсутствовать функция main(), а сам файл должен называться biginteger.h.
В качестве компилятора необходимо указывать Make. Ваш код будет вставлен посредством команды
#include<biginteger.h> в программу, содержащую тесты; вследствие этого, код необходимо отправлять
в файле со строго соответствующим именем!*/

#include <iostream>
#include <string>
#include <vector>

using std::vector;
using std::string;
using std::ostream;
using std::istream;
using std::cout;
using std::endl;


class BigInteger {
private:
	vector<int> array_numbers; // array to store parts of BigInteger number
	bool sign; // responds to the sign of number (if true - positive, else - negative)
public:
	BigInteger() : sign(true) {	}; // default constructor
	BigInteger(int a); // constructor from int value
	BigInteger(std::string s); // constructor from string value
	explicit operator bool() {
		if (*this != 0)
			return true;
		return false;
	}
	void delete_zeros_in_front();
	void subtration(BigInteger &a, const BigInteger &b);
	void sum(BigInteger &a, const BigInteger &b);
	int compare(const BigInteger &a, int k = 1) const;
	bool operator ==(BigInteger const &b) const { return compare(b) == 0; }; //equal
	bool operator !=(BigInteger const &b) const { return compare(b) != 0; }; // not equal
	bool operator <(BigInteger const &b) const { return compare(b) == -1; }; // less
	bool operator >(BigInteger const &b) const { return compare(b) == 1; }; // more
	bool operator <=(BigInteger const &b) const { return ((compare(b) == -1) || (compare(b) == 0)); }; // less or equal
	bool operator >=(BigInteger const &b) const { return ((compare(b) == 1) || (compare(b) == 0)); }; // more or equal
	const string toString() const;
	friend const BigInteger operator +(const BigInteger &a, const BigInteger &c); // sums two BigInt numbers
	friend const BigInteger operator -(const BigInteger &a, const BigInteger &c); // subtract two BigInt numbers
	friend const BigInteger operator *(const BigInteger &b, const BigInteger &a); // multiply to BigInt numbers for O(n^2)
	friend BigInteger operator /(const BigInteger &a, const BigInteger &b); // div for two BigInt numbers
	friend const BigInteger operator %(const BigInteger &a, const BigInteger &b); // mod for two Bigint numbers
	friend istream &operator >> (istream &string_in, BigInteger &number); // operator for input from the stream
	friend ostream &operator<<(ostream &string_out, const BigInteger &number); // operator for output to the stream
	BigInteger operator *=(const BigInteger &b) {
		BigInteger ans;
		ans = *this * b;
		return *this = ans;
	}
	BigInteger& operator +=(const BigInteger &c) {
		if ((sign && c.sign) || (!sign && !c.sign)) {
			sum(*this, c);
			return *this;
		}
		if ((sign && !c.sign) || (!sign && c.sign)) {
			if (this->compare(c, 0) >= 0)
			{
				subtration(*this, c);
				return *this;
			}
			else
			{
				BigInteger tmpa = *this;
				BigInteger tmpb = c;
				subtration(tmpb, tmpa);
				return *this = tmpb;
			}
		}
		return *this;
	}
	BigInteger & operator %=(const BigInteger &b) { return *this = *this % b; };
	BigInteger & operator /=(const BigInteger &b) {
		BigInteger temp = *this;
		return *this = temp / b;
	}
	BigInteger & operator ++ () {
		*this += 1;
		return *this;
	}
	const BigInteger operator ++(int) {
		BigInteger temp = *this;
		++(*this);
		return temp;
	}
	BigInteger & operator --() {
		*this -= 1;
		return *this;
	}
	const BigInteger operator --(int) {
		BigInteger temp = *this;
		--(*this);
		return temp;
	}
	BigInteger abs() const {
		BigInteger temp = *this;
		temp.sign = true;
		return temp;
	}
	const BigInteger operator -() {
		BigInteger b = *this;
		b.sign = !this->sign;

		// case for zero number
		if ((array_numbers.size() == 1) && (array_numbers[0] == 0)) {
			b.sign = true;
			b.array_numbers[0] = 0;
			return b;
		}
		else
			return b;
	}
	BigInteger & operator -=(const BigInteger &c) {
		if ((sign && c.sign) || (!sign && !c.sign)) {
			if (this->compare(c, 0) >= 0) {
				subtration(*this, c);
				return *this;
			}
			else {
				BigInteger tmp_a(0);
				BigInteger tmp_b(0);
				tmp_a = *this;
				tmp_b = c;
				subtration(tmp_b, tmp_a);
				tmp_b.sign = !tmp_b.sign;
				return *this = tmp_b;
			}
		}
		else {
			sum(*this, c);
			return *this;
		}
		return *this;
	}
};

inline void BigInteger::delete_zeros_in_front() {
	// do while found the first digit not equal to zero or array is empty
	while (*array_numbers.rbegin() == 0) {
		array_numbers.pop_back();
		if (array_numbers.empty())
			break;
	}

	// case of empty array
	if (array_numbers.empty()) {
		sign = true;
		array_numbers.push_back(0);
	}
}

inline void BigInteger::subtration(BigInteger &a, const BigInteger &b) {
	// temp - remainder
	int temp = 0;
	for (unsigned int i = 0; i < b.array_numbers.size() || temp; ++i) {
		a.array_numbers[i] -= temp + ((i < b.array_numbers.size()) ? b.array_numbers[i] : 0);
		temp = a.array_numbers[i] < 0;

		if (temp)
			a.array_numbers[i] += 10;
	}

	// normalize
	a.delete_zeros_in_front();
	return;
}

inline void BigInteger::sum(BigInteger &a, const BigInteger &b) {
	// reminder
	int temp = 0;
	for (unsigned int i = 0; i < (a.array_numbers.size() > b.array_numbers.size() ? a.array_numbers.size() : b.array_numbers.size()) || temp; ++i) {
		if (i == a.array_numbers.size())
			a.array_numbers.push_back(0);
		a.array_numbers[i] += temp + (i < b.array_numbers.size() ? b.array_numbers[i] : 0);

		temp = a.array_numbers[i] / 10;

		a.array_numbers[i] %= 10;
	}

	// normalize
	a.delete_zeros_in_front();
	return;
}

inline int BigInteger::compare(const BigInteger &a, int k) const {
	// trivial implementation

	int check = 1;
	if (k == 1) {
		if (sign && !a.sign) return 1; // number > a
		if (!sign && a.sign) return -1; // number < a
		if (!sign && !a.sign) check = -1;
	}

	if (array_numbers.size() < a.array_numbers.size()) return (-1 * check);
	if (array_numbers.size() > a.array_numbers.size()) return check;

	for (unsigned int i(array_numbers.size()); i > 0; --i) {
		if (array_numbers[i - 1] < a.array_numbers[i - 1]) return -1 * check;
		if (array_numbers[i - 1] > a.array_numbers[i - 1]) return check;
	}

	return 0;
}

inline const string BigInteger::toString() const {
	// negative number
	string s = "";
	if (!sign)
		s = "-";

	// zero number
	if ((array_numbers[0] == 0) && (array_numbers.size() == 1)) {
		s = '0';
		return s;
	}

	bool zero = true;
	for (int i = int(array_numbers.size()) - 1; i >= 0; --i) {
		if (array_numbers[i] == 0 && zero == true) // skip first zeros
			continue;
		else {
			zero = false;
			s += array_numbers[i] + '0';
		}
	}

	// case of empty string
	if (s.empty())
		s = '0';

	// case of '-0'
	if ((s[0] == '-') && (s.length() == 1))
		s = '0';

	return s;
}

BigInteger::BigInteger(int a) : sign(true) {
	// if number to fill is negative, sign is already right,
	// but it needs to be inverted to work correctly
	if (a < 0) {
		sign = false;
		a = a * (-1);
	}
	// if is equal to zero, just push zero back
	if (a == 0) {
		array_numbers.push_back(0);
		sign = true;
	}
	// fill the array of numbers that stands for BigInteger with values
	while (a != 0) {
		array_numbers.push_back(a % 10);
		a /= 10;
	}
}

BigInteger::BigInteger(string s) : sign(true) {
	// the first element of the string may contain sign of the number to fill
	// if the number to fill is negative, '-' is in front,
	// else nothing is in front

	// fill the array with numbers
	int length = s.length() - 1;
	for (int i = length; i > 0; --i) {
		s[i] -= '0';
		array_numbers.push_back(static_cast<int>(s[i]));
	}

	if (s[0] == '-')
		sign = false;
	else {
		s[0] -= '0';
		array_numbers.push_back(static_cast<int>(s[0]));
	}
}

// subtracts two BigInt numbers
inline const BigInteger operator -(const BigInteger &a, const BigInteger &b) {
	BigInteger temp = a;
	return temp -= b;
}

// sums two BigInt numbers
const BigInteger operator +(const BigInteger &a, const BigInteger &b) {
	BigInteger temp = a;
	return temp += b;
}

// multiply to BigInt numbers for O(n^2)
inline const BigInteger operator *(const BigInteger &b, const BigInteger &a)
{
	int first_size = a.array_numbers.size();
	int second_size = b.array_numbers.size();
	// expected length for the number
	int length = first_size + second_size + 1;
	// create variable for the answer for product
	BigInteger ans;
	// resize initial array for it
	ans.array_numbers.resize(length, 0);

	// typical multiplication
	for (int i = 0; i < first_size; ++i) {
		for (int j = 0; j < second_size; ++j) {
			ans.array_numbers[i + j] += a.array_numbers[i] * b.array_numbers[j];
		}
	}

	// normalize
	for (int i = 0; i < length; ++i)
	{
		if (i < length - 1)
			ans.array_numbers[i + 1] += ans.array_numbers[i] / 10;
		ans.array_numbers[i] %= 10;
	}

	// estimate the sign for the product
	if (a.sign == b.sign)
		ans.sign = true;
	else
		ans.sign = false;

	//let's remove Zeroes from the answer
	ans.delete_zeros_in_front();
	return ans;
}

// div for two BigInt numbers
inline BigInteger operator /(const BigInteger &a, const BigInteger &b)
{
	int size_divider = a.array_numbers.size();

	// create a variable for the answer
	BigInteger ans;
	// it is obvious that the size of the quotient
	// cannot be bigger than size_divider
	ans.array_numbers.resize(size_divider);

	// estimate the sign for the product
	if (a.sign == b.sign)
		ans.sign = true;
	else
		ans.sign = false;

	BigInteger temp_value;

	// let's make a bar division
	for (int i = int(size_divider) - 1; i >= 0; --i)
	{
		temp_value.array_numbers.insert(temp_value.array_numbers.begin(), 0);

		// delete useless zeros in front
		temp_value.delete_zeros_in_front();
		temp_value.array_numbers[0] = a.array_numbers[i];
		int digit = 0;

		// borders for the search with numbers with base = 10
		int l = 0, r = 9;

		// binary search
		while (l <= r)
		{
			int m = (l + r) / 2;
			BigInteger cur = b.abs() * m;
			if (cur.compare(temp_value, 0) <= 0)
			{
				digit = m;
				l = m + 1;
			}
			else
				r = m - 1;
		}
		ans.array_numbers[i] = digit;
		temp_value = temp_value - (b.abs() * digit);
	}

	// delete useless zeros in front
	ans.delete_zeros_in_front();
	return ans;
}

// mod for two BigInt numbers
inline const BigInteger operator % (const BigInteger &a, const BigInteger &b)
{
	BigInteger ans;
	ans.array_numbers.resize(a.array_numbers.size());

	BigInteger temp_value;

	for (int i = a.array_numbers.size() - 1; i >= 0; --i)
	{
		temp_value.array_numbers.insert(temp_value.array_numbers.begin(), 0);

		// delete useless zeros in front
		temp_value.delete_zeros_in_front();
		temp_value.array_numbers[0] = a.array_numbers[i];
		int digit = 0;

		// borders for the search with numbers with base = 10
		int l = 0, r = 9;

		// binary search
		while (l <= r)
		{
			int m = (l + r) / 2;
			BigInteger cur = b.abs() * m;
			if (cur.compare(temp_value, 0) <= 0)
			{
				digit = m;
				l = m + 1;
			}
			else
				r = m - 1;
		}
		ans.array_numbers[i] = digit;

		temp_value = temp_value - (b.abs() * digit);
	}

	// delete useless zeros in front
	temp_value.delete_zeros_in_front();

	// estimate the sign for the product
	if (a.sign)
		temp_value.sign = true;
	else
		temp_value.sign = false;

	// if it occured that mod is eqal to zero, make it zero
	if (temp_value.array_numbers.empty())
		temp_value.array_numbers.push_back(0);

	return temp_value;
}

// operator for input from the stream
// (used some materials from Habr to write this operator)
inline istream &operator >> (istream &string_in, BigInteger &number)
{
	string s;
	string_in >> s;
	BigInteger b(s);
	number = b;
	return string_in;
}

// operator for output to the stream
// (used some materials from Habr to write this operator)
inline ostream &operator <<(ostream &string_out, const BigInteger &number) {
	string s = number.toString();
	string_out << s;
	return string_out;
}