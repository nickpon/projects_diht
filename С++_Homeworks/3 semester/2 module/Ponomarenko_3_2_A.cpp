﻿// Ponomarenko N. 691
// A. Дана строка длины n. Найти количество ее различных подстрок.
//Используйте суффиксный массив.
//Построение суффиксного массива выполняйте за O(n log n).
//Вычисление количества различных подстрок выполняйте за O(n).

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using std::vector;
using std::string;
using std::max;
using std::cin;
using std::cout;

struct suffix {
	int index; // index of the suffix in the string given
	vector <int> rank; // is needed to sort
};

// l - left border of radix_sort, r - right border of radix_sort, d - the current digit (разряд),
// on which sorting is performed.
void radix_sort(vector<suffix> & suffixes, int l, int r, int d, int str_len) {
	if ((d > 1) || (l >= r))
		return;
	// we have alphabet formed on 26 words of Engkish alphabet + '-1', that stands for the
	// absence of the symbol
	vector <int> cnt(28);
	// count the number of each symbol
	for (int i = l; i < r; ++i) {
		int j = suffixes[i].rank[d];
		++cnt[j + 2];
	}
	// form boarders
	for (int j = 2; j < 28; ++j)
		cnt[j] += cnt[j - 1];
	vector <suffix> c(str_len); // auxiliary array
								// find the position for each element depending on it's rank[d]
	for (int i = l; i < r; ++i) {
		int j = suffixes[i].rank[d];
		c[l + cnt[j + 1]] = suffixes[i];
		// as the symbol added, increase the left border for the remaining elements
		// in this very gap
		++cnt[j + 1];
	}
	// rewrite the suffix array already sorted by the first character
	for (int i = l; i < (r); ++i)
		suffixes[i] = c[i];
	// sort the first group of elements which have the same the least rank[d]
	radix_sort(suffixes, l, l + cnt[0], d + 1, str_len);
	// sort other groups
	for (int i = 1; i < 28; ++i)
		radix_sort(suffixes, l + cnt[i - 1], l + cnt[i], d + 1, str_len);
}

vector <int> build_suffix_array(string str) {
	int str_len = str.length();
	vector <int> suffix_array(str_len, 0);
	vector <suffix> suffixes(str_len);
	// Fill suffixes array with all suffixes from the string given.
	// For each suffix we assign it's index, the first letter into
	// rank[0], the second letter into rank[1] 
	for (int i = 0; i < str_len; ++i) {
		suffixes[i].index = i;
		suffixes[i].rank.resize(2);
		suffixes[i].rank[0] = static_cast<int>(str[i] - 'a');
		if ((i + 1) < str_len)
			suffixes[i].rank[1] = static_cast<int>(str[i + 1] - 'a');
		else // in case of absence the next character
			suffixes[i].rank[1] = -1;
	}
	// sort these substrings according to the first two characters
	radix_sort(suffixes, 0, str_len, 0, str_len);
	// This array is needed to get the index in suffixes[] from original index.
	vector <int> ind(str_len, 0);
	for (int k = 4; k < (2 * str_len); k *= 2) {
		// assigning rank and index values to the first suffix
		int rank = 0;
		int prev_rank = suffixes[0].rank[0];
		suffixes[0].rank[0] = rank;
		ind[suffixes[0].index] = 0;
		// assigning rank to suffixes
		for (int i = 1; i < str_len; ++i) {
			// if first rank and next ranks are same as that of previous
			// suffix in array, assign the same new rank to this suffix
			if ((suffixes[i].rank[0] == prev_rank) && (suffixes[i].rank[1] == suffixes[i - 1].rank[1])) {
				prev_rank = suffixes[i].rank[0];
				suffixes[i].rank[0] = rank;
			}
			else { // Otherwise increment rank and assign
				prev_rank = suffixes[i].rank[0];
				suffixes[i].rank[0] = ++rank;
			}
			ind[suffixes[i].index] = i;
		}
		// assign next rank to every suffix
		for (int i = 0; i < str_len; ++i) {
			int nextindex = suffixes[i].index + k / 2;
			if (nextindex < str_len)
				suffixes[i].rank[1] = suffixes[ind[nextindex]].rank[0];
			else
				suffixes[i].rank[1] = -1;
		}
		// sort these substrings according to the first k characters
		radix_sort(suffixes, 0, str_len, 0, str_len);
	}
	for (int i = 0; i < str_len; ++i)
		suffix_array[i] = suffixes[i].index;
	return suffix_array;
};

// Kasai algorithm.
// lcp[i] - the length of the biggest common prefix of suffix_array[i] and suffix_array[i + 1].
// Baised on the article from neerc.ifmo.ru
vector<int> lcp(string & str, vector<int> & suffix_array)
{
	int n = str.length();
	vector<int> lcp(n, 0);
	vector<int> pos(n, 0);
	for (int i = 0; i < n; ++i) pos[suffix_array[i]] = i;
	for (int i = 0, k = 0; i < n; ++i)
	{
		if (k > 0)
			k--;
		if (pos[i] == n - 1) {
			lcp[n - 1] = -1;
			k = 0;
		}
		else {
			int j = suffix_array[pos[i] + 1];
			while ((max(i + k, j + k) < n) && (str[i + k] == str[j + k]))
				k++;
			lcp[pos[i]] = k;
		}
	}
	return lcp;
}

// Every suffix that stands on the i-th postion can provide as with str_len - suffix_array[i] substrings.
// We ought to count only those substrings which do not coincide with prefixes from the next
// suffix in the suffix_array. Therefore for i in {0, ..., str_len - 1} the number of new substrings formed
// is (str_len - suffix_array[i] - lcp_array[i]), where lcp_array[i] shows the number of coincidences.
// We recognize the last suffix in the suffix_array as a suffix that do not coincide with others,
// that is why it can form str_len - suffix_array[str_len - 1] substrings.
// The sum of all the prefixes will be the final answer to the problem stated.
int result(vector<int> & lcp_array, vector<int> & suffix_array, int str_len) {
	int result = 0;
	for (int i = 0; i < (str_len - 1); ++i)  // there are only (n - 1) pairs of neighbouring elements 
		result += (str_len - suffix_array[i] - lcp_array[i]);
	result += str_len - suffix_array[str_len - 1]; // count the last substring
	return result;
}

int main()
{
	string str = "";
	cin >> str;
	int str_len = str.length();
	vector <int> suffix_array(str_len, 0);
	vector <int> lcp_array(str_len, 0);
	suffix_array = build_suffix_array(str);
	lcp_array = lcp(str, suffix_array);
	cout << result(lcp_array, suffix_array, str_len);
	return 0;
}