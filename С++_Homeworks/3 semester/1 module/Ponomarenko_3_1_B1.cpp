﻿/* Пономаренко Николай 691
Найти лексикографически-минимальную строку, построенную по префикс-функции, в алфавите a-z.*/

#include <iostream>
#include <vector>
#include <string>
using namespace std;

vector<int> Prefix(string s) {
	vector <int> v(s.length());
	for (int i = 1; i < s.length(); ++i) {
		int k = v[i - 1];
		while ((k > 0) && (s[k] != s[i]))
			k = v[k - 1];
		if (s[k] == s[i])
			++k;
		v[i] = k;
	}
	return v;
}

string buildFromPrefix(vector <int> prefix) {
	string s = "a"; // minimum string will always start with "a", s - result string
	for (int i = 1; i < prefix.size(); ++i) { // start from 1 because zero position is defined
		if (prefix[i] == 0) {
			int k = prefix[i - 1]; // algorithm used as in the prefix-function 
			vector <int> used(26, 0); // array of the letters previously used in the string
			while (k > 0) {
				used[static_cast<int>(s[k]) - 97] = 1; // save all the letters found
				k = prefix[k - 1];
			}
			for (int j = 1; j < 26; ++j) { // finds out the least letter that wasn't used and puts it in the end of the string
				if (used[j] == 0) {
					s += static_cast<char>(j + 97);
					break;
				}
			}
		}
		else
			s += s[prefix[i] - 1];
	}
	return s;
}

int main()
{
	vector <int> q;
	int num = 0;
	while (cin >> num)
		q.push_back(num);
	cout << buildFromPrefix(q);
	return 0;
}