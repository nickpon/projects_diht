﻿/*Пономаренко Николай 691
Шаблон поиска задан строкой длины m, в которой кроме обычных символов могут встречаться символы “?”.
Найти позиции всех вхождений шаблона в тексте длины n.
Каждое вхождение шаблона предполагает, что все обычные символы совпадают с
соответствующими из текста, а вместо символа '?'
в тексте встречается произвольный символ.
Время работы - O(n + m + Z), где Z - общее -число вхождений подстрок шаблона
'между вопросиками' в исходном тексте. (Ахо-Корасик)
*/

#include<iostream>
#include<string>
#include <vector>

// Functions for building suffix links are made with a help of https://habrahabr.ru/post/198682/

struct bohr_vert {
public:
	std::vector<int> next_vert; // to which vert. we can go from v vert.
	std::vector<int> auto_move; // for suffix links of v vert.
	std::vector<int> pos_in_big_str;
	bool leaf; // is leaf (end of the string) or not
	int par; // parent
	int sym; // symbol
	int suf_link; // suffix link
	int up_link; // suffix link (for good ones)
	bohr_vert() : leaf(false), // intializing the root
		par(0),
		sym(0),
		suf_link(-1),
		up_link(-1) {
		next_vert.assign(26, -1); // 26 letters in our alphabet
		auto_move.assign(26, -1);
	};
	bohr_vert(int num, char c) : leaf(false), // intitializing the new vert. in bohr
		par(num), // num - number of it's parent
		sym(c), // c is given to the edge from parent to this very vert.
		suf_link(-1),
		up_link(-1) {
		next_vert.assign(26, -1);
		auto_move.assign(26, -1);
	}
};

class Aho_Korasic_mask {
public:
	Aho_Korasic_mask() { }; // simple initializer
	Aho_Korasic_mask(std::string & str); // builds bohr from the string (with '?') given
	void Add_string(std::string & str, int index); // adds string to the bohr
	std::vector<int> Find_strings(std::string & str); // function for the final answer
	int get_auto_move(int v, char ch); // array of possible movements for v vert.
	int get_suff_link(int v); // array of suffix linkes for v vert.
	int get_suff_flink(int v); // array of fsuffix linkes (good ones) for v vert.
private:
	std::vector<std::string> pattern; // stores all substrings of the initial string
	std::vector<int> ends; // positions of the ends of the substrings in initial string
	std::vector<bohr_vert> bohr; // array of bohr vert.
	int pattern_len; // the length of needle
	int vert_in_bohr_counter; // shows the number of vert. in bohr
};

Aho_Korasic_mask::Aho_Korasic_mask(std::string & str) : vert_in_bohr_counter(1), // always bohr has at least root
pattern_len(str.length()) {
	std::string temp = "";
	int i = 0;
	// finds all patterns
	while (i < str.length()) {
		if (str[i] == '?') {
			++i;
			continue;
		}
		while ((str[i] != '?') && (i < str.length())) {
			temp += str[i++];
		}
		pattern.push_back(temp);
		temp = "";
	}
	//finds all positions of the ends of the patterns in the initial string
	for (int i = 0; i < (str.length() - 1); ++i) {
		if ((str[i] != '?') && (str[i + 1] == '?'))
			ends.push_back(i + 1);
	}
	// checks the variant with a last symbol in the initial line
	if (str[str.length() - 1] != '?')
		ends.push_back(str.length());

	bohr_vert root; // creates root
	bohr.push_back(root); // puts it to the bohr
	for (int i = 0; i < pattern.size(); ++i) // adds strings to the bohr
		Add_string(pattern[i], i);
};

void Aho_Korasic_mask::Add_string(std::string & str, int index) {
	int num = 0;
	for (int i = 0; i < str.length(); ++i) {
		int ch = str[i] - 'a';
		if (bohr[num].next_vert[ch] == -1) { // if there is now needed vert., create it
			bohr_vert new_bohr_vert(num, ch);
			bohr[num].next_vert[ch] = vert_in_bohr_counter++;
			bohr.push_back(new_bohr_vert);
		}
		num = bohr[num].next_vert[ch]; // go to the vert. needed
	}
	bohr[num].leaf = true; // marks that the last symbol of the pattern is a leaf
	bohr[num].pos_in_big_str.push_back(ends[index]); // remembers the end position of the pattern in the initial line
}

std::vector<int> Aho_Korasic_mask::Find_strings(std::string & str) {
	std::vector<int> answer;
	if (str.length() < pattern_len) // it is the most bizzare exeption that ought to be tested
		return answer;
	std::vector<int> C(str.length(), 0); // the number of unmasked substrings of the pattern encountered in the text,
										 // which begins in the text at position i
	int index = 0;
	for (int i = 0; i < str.length(); ++i) {
		char ch = str[i];
		index = get_auto_move(index, ch);
		int temp = index;
		while (temp != 0) {
			if (bohr[temp].leaf) { // substring in haystack found
				for (auto x : bohr[temp].pos_in_big_str) // can the needle be placed in the haystack from this very position
					if (i - x + 1 >= 0) {
						++C[i - x + 1];
					}
			}
			temp = get_suff_flink(temp);
		}
	}
	for (int j = 0; j < C.size() - pattern_len + 1; ++j) {
		if (C[j] == pattern.size())
			answer.push_back(j);
	}
	return answer;
}

int Aho_Korasic_mask::get_suff_link(int v) {
	if (bohr[v].suf_link == -1) // if suffix link has not already been count
		if (v == 0 || bohr[v].par == 0) // if root or son of it
			bohr[v].suf_link = 0; // suffix link goes straight to the root
		else
			bohr[v].suf_link = get_auto_move(get_suff_link(bohr[v].par), char(bohr[v].sym + 97)); // tries to find substring needed for suffix link
	return bohr[v].suf_link;
}

int Aho_Korasic_mask::get_auto_move(int v, char ch) {
	if (bohr[v].auto_move[(int)ch - 97] == -1) // if the way is not found
		if (bohr[v].next_vert[(int)ch - 97] != -1) // is there is a way in bohr to ste string needed
			bohr[v].auto_move[(int)ch - 97] = bohr[v].next_vert[(int)ch - 97];
		else
			if (v == 0) // if root
				bohr[v].auto_move[(int)ch - 97] = 0;
			else // use suffix links
				bohr[v].auto_move[(int)ch - 97] = get_auto_move(get_suff_link(v), ch);
	return bohr[v].auto_move[(int)ch - 97]; // return the way found
}

int Aho_Korasic_mask::get_suff_flink(int v) {
	if (bohr[v].up_link == -1) { // if good suffix link has not already been found
		int u = get_suff_link(v);
		if (u == 0) // root or a son of it
			bohr[v].up_link = 0;
		else
			bohr[v].up_link = (bohr[u].leaf) ? u : get_suff_flink(u); // if leaf - return this suffix link, else try the next one
	}
	return bohr[v].up_link;
}

int main() {
	std::string needle = ""; // (the string with '?')
	std::string haystack = ""; // the string in which we search
	std::cin >> needle >> haystack;
	Aho_Korasic_mask my_class(needle); // builds bohr
	std::vector<int> ans = my_class.Find_strings(haystack); // finds answer
	for (auto i : ans)
		std::cout << i << " ";
}