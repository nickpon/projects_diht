﻿/* Пономаренко Николай 691
Найти лексикографически-минимальную строку, построенную по z-функции, в алфавите a-z.*/

#include <iostream>
#include <vector>
#include <string>

// Algorithms are taken from neerc.ifmo.ru or Korman's book
// I can remake some algorithms if needed.

class Strings {
public:
	Strings() {}; // itializer
	Strings(std::vector<int> _numbers); // initilizer with conditions
	std::vector<int> Prefix();
	std::vector<int> ZFunc();
	std::vector<int> FromZFuncToPrefix();
	std::vector<int> FromPrefixToZFunc();
	std::string buildFromPrefix();
	std::string buildFromZFunc();
private:
	std::vector<int> numbers; // to store numbers for some methods
	std::string str; // to store string for some methods
};

Strings::Strings(std::vector <int> _numbers) {
	numbers.resize(_numbers.size());
	numbers = _numbers;
	str = "";
}

std::vector<int> Strings::Prefix() {
	std::string s = str;
	std::vector <int> pi(s.length()); // array for Prefix function
	for (int i = 1; i < s.length(); ++i) { // pi[0] = 0 (we define it this way); pefix function algorithm
		int j = pi[i - 1];
		while ((j > 0) && (s[j] != s[i])) j = pi[j - 1];
		if (s[j] == s[i]) ++j;
		pi[i] = j;
	}
	return pi;
}

std::vector<int> Strings::ZFunc() { // typical Z-Function algorithm
	std::string s = str;
	int m = s.length();
	std::vector<int> Z(m, m);
	int left = 0, right = 0; // borders of the matching segment
	for (int i = 1; i < m; ++i) {
		int l = i - left; // for making the code readable
		int r = right - i; // for making the code readable
		int min = !(Z[l] < (r)) ? (r) : (Z[l]); // finds min between Z[l] and r
		Z[i] = (0 < min) ? min : 0; // finds max between 0 and min
		while ((i + Z[i] < m) && (s[Z[i]] == s[i + Z[i]]))
			++Z[i];
		if (i + Z[i] >= right) {
			left = i;
			right = i + Z[i];
		}
	}
	return Z;
}

std::vector <int> Strings::FromZFuncToPrefix() {
	std::vector <int> Z;
	Z = numbers; // numbers are initially stored
	std::vector <int> prefix(Z.size(), 0); // prefix[0] = 0 (we define it this way)
	for (int i = 1; i < Z.size(); ++i) {
		for (int j = Z[i] - 1; j >= 0; --j) {
			if (prefix[i + j] > 0)
				break;
			else
				prefix[i + j] = j + 1;
		}
	}
	return prefix;
}

std::vector <int> Strings::FromPrefixToZFunc() {
	std::vector <int> prefix;
	prefix = numbers;
	std::vector <int> Z(prefix.size(), 0);
	for (int i = 1; i < prefix.size(); ++i) {
		if (prefix[i] > 0)
			Z[i - prefix[i] + 1] = prefix[i];
	}
	Z[0] = prefix.size();
	int i = 1;
	while (i < prefix.size()) {
		int t = i;
		if (Z[i] > 0)
			for (int j = 1; j < Z[i]; ++j) {
				if (Z[i + j] > Z[j])
					break;
				Z[i + j] = (Z[j] < (Z[i] - 1)) ? Z[j] : (Z[i] - 1); // min(Z[j], Z[i] - j);
				t = i + j;
			};
		i = t + 1;
	}
	return Z;
}

std::string Strings::buildFromPrefix() {
	std::vector <int> prefix;
	prefix = numbers;
	std::string s = "a"; // minimum string will always start with "a", s - result string
	for (int i = 1; i < prefix.size(); ++i) { // start from 1 because zero position is defined
		if (prefix[i] == 0) {
			int k = prefix[i - 1]; // algorithm used as in the prefix-function 
			std::vector <int> used(26, 0); // array of the letters previously used in the string
			while (k > 0) {
				used[static_cast<int>(s[k]) - 97] = 1; // save all the letters found
				k = prefix[k - 1];
			}
			for (int j = 1; j < 26; ++j) { // finds out the least letter that wasn't used and puts it in the end of the std::string
				if (used[j] == 0) {
					s += static_cast<char>(j + 97);
					break;
				}
			}
		}
		else
			s += s[prefix[i] - 1];
	}
	return s;
}

// converts array build on Z-function into Prefix function array,
// builds string on prefix function array
std::string Strings::buildFromZFunc() {
	std::vector <int> Z;
	Z = numbers;
	std::vector <int> prefix(Z.size(), 0);
	prefix = FromZFuncToPrefix(); // create prefix-function's array based on Z-function's array
	std::string s = "";
	numbers = prefix;
	s = buildFromPrefix(); // use algorithm made before
	return s;
}

int main()
{
	std::vector<int> stream;
	int num = 0;
	while (std::cin >> num)
		stream.push_back(num);
	Strings MyClass(stream);
	std::cout << MyClass.buildFromZFunc();
	return 0;
}